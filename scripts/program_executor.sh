#!/bin/bash

# This script is used to run a test program using starpu with energy counters
# It needs to be sourced by another script to work properly

# Variables
declare -A ENERGY_SOLVER_EN_VARS=() # Associative array for environment variables
declare -A TAGS=()                  # Associative array for tags
experiment_name=""                  # Experiment name for the current run, initially empty
experiment_folder=""                # Experiment folder for the current run, initially empty

# Helper function to parse JSON file and store key-value pairs in ENERGY_SOLVER_EN_VARS
# Usage: parse_json_to_env_vars <json_file>
parse_json_to_env_vars() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: parse_json_to_env_vars <json_file>" >&2
    return 1
  fi
  local json_file="$1"
  if [[ ! -f "$json_file" ]]; then
    echo "Error: JSON file '$json_file' does not exist." >&2
    return 1
  fi
  command -v jq >/dev/null 2>&1 || {
    echo >&2 "jq is required but not installed."
    return 1
  }
  while IFS="=" read -r key value; do
    ENERGY_SOLVER_EN_VARS["$key"]="$value"
  done < <(jq -r 'to_entries | .[] | "\(.key)=\(.value)"' "$json_file")
  if [[ ${#ENERGY_SOLVER_EN_VARS[@]} -eq 0 ]]; then
    echo "Error: No variables were loaded from $json_file." >&2
    return 1
  fi
  return 0
}

# Clear the environment variables from the associative array and unset them
# Usage: clear_env_vars
clear_env_vars() {
  for key in "${!ENERGY_SOLVER_EN_VARS[@]}"; do
    unset "$key"
    unset ENERGY_SOLVER_EN_VARS["$key"]
  done
}

# Initialize function to load default values from JSON files
# This function is re-entrant and can be called multiple times
# Usage: initialize_env_vars <program_name>
initialize_env_vars() {
  if [[ ${#ENERGY_SOLVER_EN_VARS[@]} -ne 0 ]]; then
    clear_env_vars
  fi
  local program_name=$PROGNAME_NO_EXT
  local program_conf_file="$configuration_dir/tested_applications/$program_name.json"
  parse_json_to_env_vars "$common_conf_file" || return 1
  echo "Successfully loaded common environment variables"
  parse_json_to_env_vars "$program_conf_file" || return 1
  echo "Successfully loaded $program_name environment variables"
  echo "${#ENERGY_SOLVER_EN_VARS[@]} environment variables loaded"
}

# Add or override an environment variable in the associative array
# Usage: set_env_var <key> <value>
set_env_var() {
  local key="$1"
  local value="$2"
  ENERGY_SOLVER_EN_VARS["$key"]="$value"
}

# Print the environment variables from the associative array
# Usage: show_env_vars
show_env_vars() {
  for key in "${!ENERGY_SOLVER_EN_VARS[@]}"; do
    echo "$key=${ENERGY_SOLVER_EN_VARS[$key]}"
  done
}

# Export the environment variables from the associative array
# Usage: export_env_vars
export_env_vars() {
  for key in "${!ENERGY_SOLVER_EN_VARS[@]}"; do
    export "$key=${ENERGY_SOLVER_EN_VARS[$key]}"
  done
  echo "Successfully exported ${#ENERGY_SOLVER_EN_VARS[@]} environment variables"
}

# Helper function to measure total energy from analysis of the main log file
measure_total_energy_from_main_log() {
  local main_logfile="$experiment_folder/${PROGNAME_NO_EXT}_main.log"
  local energy_recap_file="$experiment_folder/energy_recap.txt"
  total_pkg_energy=$(awk '
    /^PKG [0-9]+/ { 
        total_pkg_energy += $3
    }
    END {
        printf("%.3f", total_pkg_energy)
    }
' "$main_logfile")
  total_gpu_energy=$(awk '
    /^GPU [0-9]+/ { 
        total_gpu_energy += $3
    }
    END {
        printf("%.3f", total_gpu_energy)
    }
' "$main_logfile")
  echo "TOTAL_PKG_ENERGY=$total_pkg_energy" | tee "$energy_recap_file"
  echo "TOTAL_GPU_ENERGY=$total_gpu_energy" | tee -a "$energy_recap_file"
}

get_rapl_sample_interval_from_log() {
  local main_logfile="$experiment_folder/${PROGNAME_NO_EXT}_main.log"
  local rapl_sample_interval_ms=$(awk '
    /ENERGY_READING_RAPL_SAMPLE_INTERVAL_MS/ { 
        print $2
    }
' "$main_logfile" | cut -d '=' -f 2)
  echo "$rapl_sample_interval_ms"
}

get_nvml_sample_interval_from_log() {
  local main_logfile="$experiment_folder/${PROGNAME_NO_EXT}_main.log"
  local gpu_sample_interval_ms=$(awk '
    /ENERGY_READING_GPU_SAMPLE_INTERVAL_MS/ { 
        print $2
    }
' "$main_logfile" | cut -d '=' -f 2)
  echo "$gpu_sample_interval_ms"
}

hash_exp() {
  echo "$(hostname)-$(date +%s)" | sha256sum | base64 | head -c 8
}

is_on_oar_compute_node() {
  if [ -z "${OAR_RESOURCE_PROPERTIES_FILE+x}" ]; then
    return 1
  else
    return 0
  fi
}

get_cluster_name() {
  if is_on_oar_compute_node; then
    cluster=$(oarprint host -P cluster)
  else
    cluster=$(hostname)
  fi
  echo "$cluster"
}

get_cpu_model() {
  cpu=$(lscpu | grep "Model name" | awk -F: '{print $2}' | sed 's/^ *//')
  echo "$cpu"
}

get_gpu_model() {
  local out="NONE:0"
  local has_gpu=0
  local num_nvidia=0
  local num_amd=0

  for device in /sys/class/drm/card*/device/vendor; do
    if [[ -f "$device" ]]; then
      local vendor_id
      vendor_id=$(cat "$device" 2>/dev/null)
      case "$vendor_id" in
      "0x10de")
        has_gpu=1
        num_nvidia=$((num_nvidia + 1))
        ;;
      "0x1002")
        has_gpu=1
        num_amd=$((num_amd + 1))
        ;;
      esac
    fi
  done

  if ((num_nvidia > 0 && num_amd > 0)); then
    out="CONFLICT:?"
  elif ((num_nvidia > 0)); then
    out="NVIDIA:$num_nvidia"
  elif ((num_amd > 0)); then
    out="AMD:$num_amd"
  fi
  echo "$out"
}

get_ram_info() {
  if command -v free >/dev/null 2>&1; then
    free -h | grep Mem | awk '{print $2}'
  else
    echo "N/A"
  fi
}

add_tag() {
  if [ $# -ne 2 ]; then
    echo "Usage: add_tag <key> <value>"
    return 1
  fi
  local key="$1"
  local value="$2"
  TAGS["$key"]="$value"
  return 0
}

dump_tags() {
  if [ $# -ne 1 ]; then
    echo "Usage: dump_tags <tags_file>"
    return 1
  fi
  local tags_file="$1"
  if [ ${#TAGS[@]} -eq 0 ]; then
    echo "{}" >"$tags_file"
    return 0
  fi
  local json="{"
  for key in "${!TAGS[@]}"; do
    json+="\"$key\":\"${TAGS[$key]}\","
  done
  json="${json%,}}"
  echo "$json" | jq . >"$tags_file"
  return 0
}

dump_env_vars() {
  if [ $# -ne 1 ]; then
    echo "Usage: dump_env_vars <env_file>"
    return 1
  fi
  local env_file="$1"
  if [ ${#ENERGY_SOLVER_EN_VARS[@]} -eq 0 ]; then
    echo "{}" >"$env_file"
    return 0
  fi
  local json="{"
  for key in "${!ENERGY_SOLVER_EN_VARS[@]}"; do
    json+="\"$key\":\"${ENERGY_SOLVER_EN_VARS[$key]}\","
  done
  json="${json%,}}"
  echo "$json" | jq . >"$env_file"
  return 0
}

show_tags() {
  for key in "${!TAGS[@]}"; do
    echo "$key:${TAGS[$key]}"
  done
}

# Execute the test program in a new experiment folder with custom environment variables
# Usage : execute_test_program [args]
execute_test_program() {
  local hashed_exp_code=$(hash_exp)
  local cluster_name=$(get_cluster_name)
  local host_name=$(hostname)
  experiment_name="${PROGNAME_NO_EXT}_${cluster_name}_${hashed_exp_code}"
  local program_args="$@"
  if [ ! -z ${ENERGY_SOLVER_DATA_DIR+x} ]; then
    experiments_dir="${ENERGY_SOLVER_DATA_DIR}/experiments"
    mkdir -p "$experiments_dir"
    experiment_folder="${experiments_dir}/${experiment_name}"
  else
    experiments_dir="${root_dir}/results/experiments"
    experiment_folder="${experiments_dir}/${experiment_name}"
  fi
  echo "Experiment data will be saved to $experiment_folder"
  mkdir -p "$experiment_folder"
  local fxt_results_folder="$experiment_folder/fxt_results"
  mkdir -p "$fxt_results_folder"
  set_env_var "STARPU_FXT_PREFIX" "$fxt_results_folder"
  local builder_results_dir="$experiment_folder/builder_results"
  mkdir -p "$builder_results_dir"
  local solver_results_dir="$experiment_folder/solver_results"
  mkdir -p $solver_results_dir

  # Record tags
  local tags_file="$experiment_folder/tags.json"
  echo $(dump_tags "$tags_file") || return 1

  # Export and save environment variables for this program
  local environment_file="$experiment_folder/environment.json"
  touch "$environment_file"
  dump_env_vars "$environment_file" || return 1
  export_env_vars

  # Record hardware information
  local host_infos_file="$experiment_folder/host_infos.txt"
  echo "HOST_NAME=$host_name" >"$host_infos_file"
  echo "CLUSTER_NAME=$(get_cluster_name)" >>"$host_infos_file"
  echo "CPU_MODEL=$(get_cpu_model)" >>"$host_infos_file"
  echo "GPU_MODEL=$(get_gpu_model)" >>"$host_infos_file"
  echo "RAM=$(get_ram_info)" >>"$host_infos_file"

  # Run the program
  echo "Running test program : $PROGNAME"
  local main_logfile="$experiment_folder/${PROGNAME_NO_EXT}_main.log"
  local start_time=$(date +%s.%N)
  source $tested_apps_dir/$PROGNAME $program_args 2>&1 | tee "$main_logfile"
  if [ ${PIPESTATUS[0]} -ne 0 ]; then
    echo "Cleaning up experiment folder $experiment_folder"
    prof_file="$fxt_results_folder/raw_prof_file_0"
    if [ -f "$prof_file" ]; then
      rm -f "$prof_file"
    fi
    rm -f "$main_logfile" "$environment_file" "$host_infos_file" "$tags_file"
    rm -d "$builder_results_dir" "$solver_results_dir" "$fxt_results_folder"
    rm -d "$experiment_folder"
    return 1
  fi
  local end_time=$(date +%s.%N)
  local prog_time=$(awk "BEGIN {printf \"%.3f\", $end_time - $start_time}")
  echo "Program ran in $prog_time seconds for $experiment_name"

  # Record total energy consumption
  measure_total_energy_from_main_log || return 1

  # Record experiment informations
  local experiment_infos_file="$experiment_folder/experiment_infos.txt"
  echo "PROGRAM=$PROGNAME" >"$experiment_infos_file"
  echo "DURATION=$prog_time" >>"$experiment_infos_file"

  # Record energy reading intervals
  local rapl_sample_interval=$(get_rapl_sample_interval_from_log)
  local nvml_sample_interval=$(get_nvml_sample_interval_from_log)
  echo "RAPL_SAMPLE_INTERVAL_MS=$rapl_sample_interval" >>"$experiment_infos_file"
  echo "GPU_SAMPLE_INTERVAL_MS=$nvml_sample_interval" >>"$experiment_infos_file"
  return 0
}

# Helper function to clean the trace data
clean_fxt_trace_data() {
  if [ -z ${experiment_folder+x} ]; then
    echo "Error: Experiment folder is not defined."
    return 1
  fi
  if [ ! -d "$experiment_folder" ]; then
    echo "Error: Experiment folder '$experiment_folder' does not exist."
    return 1
  fi
  local fxt_results_folder="$experiment_folder/fxt_results"
  rm -rf $fxt_results_folder || return 1
}

# Generate various .rec files from the raw fxt trace using starpu_fxt_tool
extract_trace_data() {
  # Extract the trace with starpu_fxt_tool
  local fxt_results_folder="$experiment_folder/fxt_results"
  cd $fxt_results_folder || return 1
  local trace_file=$(ls -1 | head -n 1)
  echo "Extracting trace $trace_file from $fxt_results_folder"
  local start_time=$(date +%s.%N)
  starpu_fxt_tool -i $trace_file 2>&1 || return 1
  cd - >/dev/null || return 1
  local end_time=$(date +%s.%N)
  local trace_time=$(awk "BEGIN {printf \"%.3f\", $end_time - $start_time}")
  echo "Extracted trace in $trace_time seconds for $experiment_name"
  return 0
}

# Create csv files from the .rec files
create_csv_files() {
  local start_time=$(date +%s.%N)
  local builder_results_dir="$experiment_folder/builder_results"
  local fxt_results_folder="$experiment_folder/fxt_results"
  $system_builder_dir/extract_tasks_infos.sh $fxt_results_folder $builder_results_dir || return 1
  $system_builder_dir/extract_energy_infos.sh $fxt_results_folder $builder_results_dir || return 1
  $system_builder_dir/extract_topo_infos.sh $fxt_results_folder $builder_results_dir || return 1
  $system_builder_dir/extract_state_infos.sh $fxt_results_folder $builder_results_dir || return 1
  local end_time=$(date +%s.%N)
  local csv_time=$(awk "BEGIN {printf \"%.3f\", $end_time - $start_time}")
  echo "Created CSV files in $csv_time seconds for $experiment_name"
  echo "Cleaning fxt trace data ..."
  clean_fxt_trace_data || return 1
  return 0
}

# Build the linear system of equations
build_system() {
  local start_time=$(date +%s.%N)
  export PYTHONPATH=$src_dir
  python3 $system_builder_dir/build_system.py $experiment_folder || return 1
  local end_time=$(date +%s.%N)
  local build_time=$(awk "BEGIN {printf \"%.3f\", $end_time - $start_time}")
  echo "Built energy linear systems in $build_time seconds for $experiment_name"
}

# Solver the linear system of equations
solve_system() {
  local start_time=$(date +%s.%N)
  export PYTHONPATH=$src_dir
  python3 $solver_dir/solve_system.py $experiment_folder || return 1
  local end_time=$(date +%s.%N)
  local solve_time=$(awk "BEGIN {printf \"%.3f\", $end_time - $start_time}")
  echo "Solved energy linear systems in $solve_time seconds for $experiment_name"
}

# Main function to run the program / extract trace / build system / solve system
# Usage: run_program [args]
run_program() {

  # 1 - Execute the test program
  progargs="$@"
  echo -e "\n[1] Running test program : $PROGNAME with args : $progargs ..."
  execute_test_program $progargs
  if [ $? -ne 0 ]; then
    echo "Error: Failed to execute the test program."
    return 1
  fi

  # 2 - Extract the trace with starpu_fxt_tool
  echo -e "\n[2] Extracting trace data using starpu_fxt_tool ..."
  extract_trace_data
  if [ $? -ne 0 ]; then
    echo "Error: Failed to extract the trace data."
    return 1
  fi

  # 3 - Transform relevant infos to CSV
  echo -e "\n[3] Generating CSV files ..."
  create_csv_files
  if [ $? -ne 0 ]; then
    echo "Error: Failed to generate CSV files."
    return 1
  fi

  # 4 - Build linear system
  echo -e "\n[4] Building linear system ..."
  build_system
  if [ $? -ne 0 ]; then
    echo "Error: Failed to build the linear system."
    return 1
  fi

  # 5 - Solve linear system
  echo -e "\n[5] Solving linear system ..."
  solve_system
  if [ $? -ne 0 ]; then
    echo "Error: Failed to solve the linear system."
    return 1
  fi
  return 0
}

get_available_test_programs() {
  local test_programs=($(ls -1 $tested_apps_dir))
  echo "${test_programs[@]}"
}

program_executor_usage() {
  echo "Usage: source program_executor <program_name>"
  echo "Available test programs: $(get_available_test_programs)"
}

# ==================== SCRIPT ENTRY POINT ====================

if [ "${BASH_SOURCE[0]}" = "${0}" ]; then
  echo "Error: script must be sourced and not executed directly."
  exit 1
fi

# Project folder configuration
root_dir=$(realpath $(dirname "${BASH_SOURCE[0]}")/..)
if [ ! -d "$root_dir" ]; then
  echo "Error: Root directory '$root_dir' does not exist."
  return 1
fi
script_dir="$root_dir/scripts"
src_dir="$root_dir/src"
tested_apps_dir="$src_dir/tested_applications"
system_builder_dir="$src_dir/system_builder"
solver_dir="$src_dir/system_solver"
database_dir="$src_dir/database"
configuration_dir="$root_dir/configuration"
common_conf_file="$configuration_dir/tested_applications/common.json"

# Arg check
if [ ! $# -eq 1 ]; then
  program_executor_usage
  return 1
fi
PROGNAME=$1
PROGNAME_NO_EXT=$(basename "$PROGNAME" .sh)

# Detect available programs
TEST_PROGRAMS=($(get_available_test_programs))
program_found=false
for program in "${TEST_PROGRAMS[@]}"; do
  if [[ "$(basename "$program")" == "$PROGNAME" ]]; then
    program_found=true
    break
  fi
done
if [[ $program_found == false ]]; then
  echo "Error: Invalid program name '$PROGNAME'."
  echo "Available test programs: ${TEST_PROGRAMS[*]}"
  return 1
fi
echo "Selected program: $PROGNAME"

# Load default values from JSON files to prepare for run 1
initialize_env_vars "$PROGNAME"
if [ $? -ne 0 ]; then
  echo "Error: Failed to initialize environment variables."
  return 1
fi

return 0
