#!/bin/bash

# This script starts the webapp

script_dir=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
root_dir=$(realpath "$script_dir/..")
src_dir="$root_dir/src"
web_src_dir="$src_dir/web_app"

export PYTHONPATH="$src_dir"
WEBAPP_PATH="$web_src_dir/app.py"
echo "Starting webapp at $WEBAPP_PATH"

python3 $WEBAPP_PATH
