#!/bin/bash

# This script allows basic interaction with the database
# Usage: ./util_database.sh <command> <args>
# Commands: 'create', 'remove', 'dump', 'reset' or 'merge <path>'.

script_dir=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
root_dir=$(realpath "$script_dir/..")
src_dir="$root_dir/src"
export PYTHONPATH="$src_dir"
db_src_dir="$src_dir/database"

python3 $db_src_dir/database_connector.py "$@"
