# energy-solver

## Description
This projects aims at extracting informations from starPU FXT traces with the energy-reader module activated.
We utilize the outputs of starpu_fxt_tools (namely the files tasks.rec, energy.rec and topo.rec) to create and 
solve an overdetermined linear equation system of tasks power consumption.

## Prerequisiste

### Common
In this order:
- energy-reader, FXT, starPU (compiled with energy-reader and fxt support)
- Python3 (see requirements.txt for required packages)

### Test programs
- chameleon (for chameleon_bench and chameleon_dtesting_batch)

## Installation
Install the test programs from root folder with :
```
mkdir build install
cd build
cmake .. -DCMAKE_PREFIX=$PWD/../install
make install
```
All results are stored by default under /data in the project root directory
You can change it by setting the environment variable ENERGY_SOLVER_DATA_DIR

## Testing
Once the test programs are compiled, you can try to execute them, generate an energy system and solve it.
Test programs configurations by environment variables are located under /configuration.

Testing can be done by first sourcing `program_executor.sh` under /scripts, which takes the name of 
the test_program to execute as only argument.

```
cd scripts
source program_executor.sh chameleon_bench
```

You can then execute the program and solver :

```
run_program
```

or follow the execution step by step :
```
execute_test_program
extract_trace_data
create_csv_files
build_system
solve_system
```

You can tweak test programs / builder / solver parameters by modifying the configuration files 
or by overloading them using the `set_env_var` function fom `program_executor.sh` :

Example with increase of the matrix size used by chameleon_bench:
```
set_env_var MATRIX_SIZE "48000"
run_program
```

## Visualize results

A test program run has the following identifier:
```
experiment_name="${tested_program}_$(hostname)_$(date +%Y%m%d_%H%M%S)"
```

The best way to visualize results is to use the project web application :
```
cd scripts
./start_webapp.sh
```

More informations are also available under `/data/$experiment_name`
- main_log.txt : A file containing the stderr/stdout output of running the tested program.
- environment.txt : A file containing the different relevant starpu/chameleon/energyreader environment variables 
set in the exec script before running the program. 
- fxt_results : A folder that contains the partial outputs of `starpu_fxt_tools` ran on the fxt trace files:
	```
	tasks.csv  energy.csv  topo.csv  trace.rec
	```
- solver_results : A folder that contains the outputs of the python based solver, contains :
	- CSV files generated from the .rec files and containing informations about :
		- The worker topology used by starpu in ``` topo.csv ```
		- The energy values read troughout the tested program execution in ``` energy.csv ```
		- The tasks executed by the different workers in ``` tasks.csv ```
	- ``` system.json ```, a JSON representation of the different overdetermined linear systems generated.
	- logs : a folder containing the solver logs, containing useful data :
		- ``` *.log ``` is the general logger, contains a description of systems variables, durations, energy values, topology, systems size and matrixes, system results and metrics, and any error/warning informations.
		- ``` *_results.log ``` a smaller logfile containing only system results and validation metrics.
	- plots : a folder containing KDE (kernel density estimate) and QQ (quantile–quantile) plots of the system results.

For aggregated results, they will be written under ./results/aggregated_stats/ and will contain kde plots of the energy/power values of different codelets over the experiences.
	