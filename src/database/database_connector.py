import os
import sys
import datetime
from typing import List, Optional, Any
from sqlalchemy.types import DateTime
from sqlalchemy import create_engine, select, ForeignKey, JSON, and_
from sqlalchemy.orm import relationship, mapped_column, make_transient, joinedload
from sqlalchemy.orm import sessionmaker, DeclarativeBase, Mapped
from sqlalchemy.exc import SQLAlchemyError
import sqlite3
import logging

# Add the parent directory to the path
FILEDIR = os.path.dirname(os.path.abspath(__file__))
SRC_DIR = os.path.abspath(os.path.join(FILEDIR, ".."))
sys.path.append(SRC_DIR)

# Import custom modules
from utils.custom_exceptions import DatabaseException  # NOQA

# Logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Database connection
DB_NAME = os.getenv("ENERGY_SOLVER_DB_NAME", "energy_solver.db")
ROOT_DIR = os.path.abspath(os.path.join(SRC_DIR, ".."))
DATA_DIR = os.getenv("ENERGY_SOLVER_DATA_DIR", os.path.join(ROOT_DIR, "data"))
ENERGY_SOLVER_DB = os.path.join(DATA_DIR, DB_NAME)
DATABASE_URL = f"sqlite:///{ENERGY_SOLVER_DB}"
ENGINE = create_engine(DATABASE_URL, echo=False)
Session = sessionmaker(bind=ENGINE)


def get_databases_in_data_dir(include_active: bool = False) -> List[str]:
    """
    Retrieve a list of database files in the data directory.

    Args:
        include_active (bool): Whether to include the active database file in the list.

    Returns:
        List[str]: A list of database file names in the data directory.
    """
    db_list = [f for f in os.listdir(DATA_DIR) if f.endswith(".db")]
    if not include_active:
        db_list.remove(os.path.basename(ENERGY_SOLVER_DB))
    return db_list


def get_active_database() -> str:
    """
    Retrieve the name of the active database file.

    Returns:
        str: The name of the active database file.
    """
    return os.path.basename(ENERGY_SOLVER_DB)


def set_database_from_name(db_name: str) -> None:
    """
    Change the database file to the specified name.

    Args:
        db_name (str): The name of the new database file.

    Returns:
        None
    """
    global ENERGY_SOLVER_DB, DATABASE_URL, ENGINE
    ENERGY_SOLVER_DB = os.path.join(DATA_DIR, db_name)
    if not (os.path.exists(ENERGY_SOLVER_DB)):
        raise FileNotFoundError(
            f"Database file {ENERGY_SOLVER_DB} does not exist.")
    DATABASE_URL = f"sqlite:///{ENERGY_SOLVER_DB}"
    ENGINE = create_engine(DATABASE_URL, echo=False)
    Session.configure(bind=ENGINE)
    logger.info(f"Database changed to {ENERGY_SOLVER_DB}.")
    return


class Base(DeclarativeBase):
    """
    Base class for SQLAlchemy declarative models.
    Provides a custom type annotation map for SQLAlchemy to handle specific Python types.

    Attributes:
        type_annotation_map (dict): A dictionary mapping Python types to SQLAlchemy
        types. In this case, it maps `dict[str, Any]` to `JSON`.
    """
    type_annotation_map = {
        dict[str, Any]: JSON
    }


class Cluster(Base):
    """
    Represents a cluster in the database.

    Attributes:
        id (str): The primary key identifier for the cluster.
        hosts (List[HostSystem]): List of host systems associated with the cluster.

    Methods:
        __repr__(): Returns a string representation of the Cluster instance.
    """
    __tablename__ = "cluster"
    id: Mapped[str] = mapped_column(primary_key=True)
    hosts: Mapped[List["HostSystem"]] = relationship(
        back_populates="cluster",
        cascade="all, delete",
        passive_deletes=True
    )

    def __repr__(self) -> str:
        repr = (f"Cluster(id={self.id})")
        return repr


class HostSystem(Base):
    """
    Represents a host system in the database.

    Attributes:
        id (str): The primary key identifier for the host system.
        cpu (Optional[str]): Description of the CPU in the host system.
        gpu (Optional[str]): Description of the GPU in the host system.
        ram (Optional[str]): Description of the RAM capacity in the host system.
        experiments (List[Experiment]): List of experiments associated with the host system.

    Methods:
        __repr__(): Returns a string representation of the HostSystem instance.
    """
    __tablename__ = "host_system"
    id: Mapped[str] = mapped_column(primary_key=True)
    cpu: Mapped[Optional[str]]
    gpu: Mapped[Optional[str]]
    ram: Mapped[Optional[str]]
    cluster_id: Mapped[Optional[str]] = mapped_column(ForeignKey("cluster.id"))
    cluster: Mapped[Optional["Cluster"]] = relationship(back_populates="hosts")
    experiments: Mapped[List["Experiment"]] = relationship(
        back_populates="host",
        cascade="all, delete",
        passive_deletes=True)

    def __repr__(self) -> str:
        repr = (f"HostSystem(id={self.id}, cpu={self.cpu}, gpu={self.gpu}, "
                f"ram={self.ram}, cluster_id={self.cluster_id}")
        return repr


class ExperimentTag(Base):
    """
    Association table for many-to-many relationship between Experiment and Tag.

    Attributes:
        experiment_id (str): Foreign key referencing the Experiment table.
        tag_id (int): Foreign key referencing the Tag table.
    """
    __tablename__ = "experiment_tag"
    experiment_id: Mapped[str] = mapped_column(
        ForeignKey("experiments.id"), primary_key=True)
    tag_id: Mapped[int] = mapped_column(ForeignKey("tag.id"), primary_key=True)


class Tag(Base):
    """
    Represents a key-value tag in the database.

    Attributes:
        id (int): Primary key identifier for the tag.
        key (str): The key of the tag.
        value (str): The value of the tag.
    """
    __tablename__ = "tag"
    id: Mapped[int] = mapped_column(primary_key=True)
    key: Mapped[str]
    value: Mapped[str]
    experiments: Mapped[list["Experiment"]] = relationship(
        "Experiment",
        secondary="experiment_tag",
        back_populates="tags"
    )

    def __repr__(self) -> str:
        return f"{self.key}:{self.value}"


class Experiment(Base):
    """
    Represents an experiment in the database.

    Attributes:
        id (str): The primary key of the experiment.
        program (str): The program measured in the experiment.
        host_id (str): The foreign key referencing the host system.
        environment (dict[str, Any]): JSON data containing relevant environment parameters.
        date (datetime.datetime): The date and time when the experiment was created.
        duration_s (int): The duration of the experiment in seconds.
        sample_intervals (dict[str, Any]): JSON data containing sample interval for energy measurements chosen at runtime.
        total_energy (dict[str, Any]): JSON data containing the total energy consumed.
        host (HostSystem): The host system associated with the experiment.
        linear_systems (List[LinearSystem]): A list of linear systems associated with the experiment.
        tags (List[Tag]): A list of tags associated with the experiment.

    Methods:
        __repr__(): Returns a string representation of the Experiment instance.
    """
    __tablename__ = "experiments"
    id: Mapped[str] = mapped_column(primary_key=True)
    program: Mapped[str]
    host_id: Mapped[str] = mapped_column(
        ForeignKey("host_system.id"), nullable=False)
    environment: Mapped[dict[str, Any]] = mapped_column(JSON)
    date: Mapped[datetime.datetime] = mapped_column(
        DateTime, default=datetime.datetime.now)
    duration_s: Mapped[int]
    sample_intervals: Mapped[dict[str, Any]] = mapped_column(JSON)
    total_energy: Mapped[dict[str, Any]] = mapped_column(JSON)
    host: Mapped[HostSystem] = relationship(back_populates="experiments")
    linear_systems: Mapped[List["LinearSystem"]] = relationship(
        back_populates="experiment",
        cascade="all, delete")
    tags: Mapped[list[Tag]] = relationship(
        "Tag",
        secondary="experiment_tag",
        back_populates="experiments"
    )

    def __repr__(self) -> str:
        return (f"Experiment(id={self.id}")


class LinearSystem(Base):
    """
    Represents a linear system in the database.

    Attributes:
        id (str): Primary key for the linear system.
        experiment_id (str): Foreign key referencing the associated experiment.
        system_name (str): Name of the linear system.
        system_type (str): Type of the linear system (e.g., "PKG" or "GPU").
        system_data (dict[str, Any]): JSON data containing the linear system equations.
        core_count (int): Number of cores used in the system.
        perc_filtered (float): Minimum percentage duration required for a task to be considered.
        tasks_data (dict[str, Any]): JSON data containing details about tasks.
        experiment (Experiment): Relationship to the Experiment model.
        solutions (List[Solution]): Relationship to the Solution model, with cascading delete.
    """
    __tablename__ = "linear_systems"
    id: Mapped[str] = mapped_column(primary_key=True)
    experiment_id: Mapped[str] = mapped_column(
        ForeignKey("experiments.id", ondelete="CASCADE"), nullable=False)
    system_name: Mapped[str]
    system_type: Mapped[str]
    system_data: Mapped[dict[str, Any]] = mapped_column(JSON)
    core_count: Mapped[int]
    perc_filtered: Mapped[float]
    tasks_data: Mapped[dict[str, Any]] = mapped_column(JSON)
    experiment: Mapped[Experiment] = relationship(
        back_populates="linear_systems")
    solutions: Mapped[List["Solution"]] = relationship(
        back_populates="linear_system",
        cascade="all, delete-orphan",
        single_parent=True)


class Solution(Base):
    """
    Represents a solution to a linear system in the database.

    Attributes:
        id (str): The primary key of the solution.
        linear_system_id (str): The foreign key referencing the associated linear system.
        method (str): The method used to solve the linear system.
        solution_data (dict[str, Any]): The solution in JSON format.
        residuals (dict[str, Any]): The residuals of the solution in JSON format.
        metrics (dict[str, Any]): The performance metrics of the solution in JSON format.
        linear_system (LinearSystem): The associated linear system object.
    """
    __tablename__ = "solutions"
    id: Mapped[str] = mapped_column(primary_key=True)
    linear_system_id: Mapped[str] = mapped_column(
        ForeignKey("linear_systems.id", ondelete="CASCADE"), nullable=False)
    method: Mapped[str]
    solution_data: Mapped[dict[str, Any]] = mapped_column(JSON)
    residuals: Mapped[dict[str, Any]] = mapped_column(JSON)
    metrics: Mapped[dict[str, Any]] = mapped_column(JSON)
    linear_system: Mapped[LinearSystem] = relationship(
        back_populates="solutions")


def create_database() -> None:
    """
    Creates the database if it does not already exist.
HostSystem
        None
    """
    os.makedirs(DATA_DIR, exist_ok=True)
    if os.path.exists(ENERGY_SOLVER_DB):
        logger.info(f"Database {ENERGY_SOLVER_DB} already exists.")
        return
    Base.metadata.create_all(ENGINE)
    logger.info(f"Database successfully created under {ENERGY_SOLVER_DB}.")


def remove_database() -> None:
    """
    Remove the ENERGY_SOLVER_DB database file if it exists.

    Returns:
        None
    """
    if os.path.exists(ENERGY_SOLVER_DB):
        os.remove(ENERGY_SOLVER_DB)
    logger.info(f"Database {ENERGY_SOLVER_DB} removed successfully.")


def does_database_exist() -> bool:
    """
    Checks if the energy solver database exists.

    Returns:
        bool: True if the database file exists, False otherwise.
    """
    return os.path.exists(ENERGY_SOLVER_DB)


def dump_database(database_file: str) -> None:
    """
    Dumps the contents of the specified SQLite database to a .sql file.

    Args:
        database_file (str): The path to the SQLite database file.

    Raises:
        DatabaseException: If an error occurs while dumping the database.
    """
    try:
        connection = sqlite3.connect(database_file)
        with open(database_file + ".sql", "w") as f:
            for line in connection.iterdump():
                f.write(f"{line}\n")
        connection.close()
        print(f"Database dumped successfully to {database_file}.sql")
    except Exception as e:
        raise DatabaseException(
            f"Dumping database {database_file} failed: {e}")


def try_insert_cluster(cluster_id: str) -> None:
    """
    Inserts a cluster into the database if it does not already exist.

    Args:
        cluster_id (str): The unique identifier of the cluster.

    Raises:
        DatabaseException: If an error occurs during the insertion.
    """
    try:
        with Session() as session, session.begin():
            if not session.scalar(select(Cluster).filter_by(id=cluster_id)):
                cluster = Cluster(id=cluster_id)
                session.add(cluster)
        logger.info(f"Cluster {cluster_id} inserted successfully.")
    except SQLAlchemyError as e:
        raise DatabaseException(f"Inserting cluster {cluster_id} failed: {e}")


def try_insert_host(host_id: str,
                    cpu: Optional[str] = None,
                    gpu: Optional[str] = None,
                    ram: Optional[str] = None,
                    cluster_id: Optional[str] = None) -> None:
    """
    Inserts a host system into the database if it does not already exist.

    Args:
        host_id (str): The unique identifier of the host system.
        cpu (str, optional): Description of the CPU in the host system.
        gpu (str, optional): Description of the GPU in the host system.
        ram (str, optional): Description of the RAM capacity in the host system.
        cluster_id (str, optional): The ID of the cluster to which the host belongs.

    Raises:
        DatabaseException: If an error occurs during the insertion.
    """
    try:
        with Session() as session, session.begin():
            if not session.scalar(select(HostSystem).filter_by(id=host_id)):
                host = HostSystem(id=host_id, cpu=cpu, gpu=gpu,
                                  ram=ram, cluster_id=cluster_id)
                session.add(host)
        logger.info(f"Host {host_id} inserted successfully.")
    except SQLAlchemyError as e:
        raise DatabaseException(f"Inserting host {host_id} failed: {e}")


def get_or_insert_tag(tag_key: str, tag_value: str) -> Tag:
    """
    Retrieve a tag from the database by key and value, or insert a new tag if it does not exist.

    Args:
        tag_key (str): The key of the tag.
        tag_value (str): The value of the tag.

    Returns:
        Tag: The Tag object corresponding to the key and value.

    Raises:
        DatabaseException: If there is an error during the retrieval or insertion process.
    """
    try:
        with Session() as session, session.begin():
            tag: Tag = session.scalar(
                select(Tag).filter_by(key=tag_key, value=tag_value))
            if not tag:
                tag = Tag(key=tag_key, value=tag_value)
                session.add(tag)
                logger.info(
                    f"Tag {tag_key}: {tag_value} inserted successfully.")
        return tag
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to get or insert tag {tag_key}={tag_value}: {e}")


def insert_experiment_tag(experiment_id: str, tag_key: str, tag_value: str) -> None:
    """
    Inserts a tag associated with an experiment into the database.

    Args:
        experiment_id (str): The ID of the experiment.
        tag_key (str): The key of the tag.
        tag_value (str): The value of the tag.

    Raises:
        DatabaseException: If there is an error during the insertion process.
    """
    try:
        with Session() as session, session.begin():
            experiment = session.scalar(
                select(Experiment).filter_by(id=experiment_id)
            )
            if not experiment:
                raise DatabaseException(
                    f"Experiment {experiment_id} does not exist."
                )
            tag: Tag = session.scalar(
                select(Tag).filter_by(key=tag_key, value=tag_value)
            )
            if not tag:
                tag = Tag(key=tag_key, value=tag_value)
                session.add(tag)
                logger.info(f"Tag {tag_key}: {tag_value} "
                            f"inserted successfully.")
            if tag in experiment.tags:
                raise FileExistsError(
                    f"Tag {tag_key}: {tag_value} already exists "
                    f"for experiment {experiment_id}."
                )
            experiment.tags.append(tag)
        logger.info(
            f"Tag {tag_key}: {tag_value} inserted "
            f"for experiment {experiment_id}."
        )
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to insert tag {tag_key}={tag_value} "
            f"for experiment {experiment_id}: {e}"
        )


def remove_experiment_tag(experiment_id: str, tag_key: str, tag_value: str) -> None:
    """
    Remove a tag associated with an experiment from the database.

    Args:
        experiment_id (str): The ID of the experiment.
        tag_key (str): The key of the tag.
        tag_value (str): The value of the tag.

    Raises:
        DatabaseException: If there is an error during the removal process.
    """
    try:
        with Session() as session, session.begin():
            experiment = session.scalar(
                select(Experiment).filter_by(id=experiment_id)
            )
            if not experiment:
                raise DatabaseException(
                    f"Experiment {experiment_id} does not exist."
                )
            tag: Tag = session.scalar(
                select(Tag).filter_by(key=tag_key, value=tag_value))
            print(tag)
            if not tag:
                raise DatabaseException(
                    f"Tag {tag_key}: {tag_value} does not exist."
                )
            if tag not in experiment.tags:
                raise DatabaseException(
                    f"Tag ({tag_key}: {tag_value}) does not exist for "
                    f"experiment {experiment_id}."
                )
            experiment.tags.remove(tag)
        logger.info(
            f"Tag {tag_key}: {tag_value} removed from "
            f"experiment {experiment_id}."
        )
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to remove tag {tag_key}={tag_value} "
            f"for experiment {experiment_id}: {e}"
        )


def insert_experiment(exp_id: str,
                      program: str,
                      host_id: str,
                      environment: dict[str, Any],
                      duration_s: int,
                      sample_intervals: dict[str, Any],
                      total_energy: dict[str, Any],
                      tags: Optional[dict[str, str]] = None) -> None:
    """
    Inserts an experiment into the database.

    Args:
        exp_id (str): The unique identifier of the experiment.
        program (str): The program measured in the experiment.
        host_id (str): The ID of the host system associated with the experiment.
        environment (dict[str, Any]): A dictionary containing relevant environment parameters.
        duration_s (int): The duration of the experiment in seconds.
        sample_intervals (dict[str, Any]): A dictionary containing sample interval for energy measurements chosen at runtime.
        total_energy (dict[str, Any]): A dictionary containing the total energy consumed.
        tags (dict[str, str]], optional): A dictionary of tags associated with the experiment.

    Raises:
        DatabaseException: If the experiment already exists or insertion fails.
    """
    try:
        tag_objs: List[Tag] = []
        for tag in tags or []:
            tag_objs.append(get_or_insert_tag(tag, tags[tag]))
        with Session() as session, session.begin():
            if session.scalar(select(Experiment).filter_by(id=exp_id)):
                raise DatabaseException(
                    f"Experiment {exp_id} already exists.")
            experiment = Experiment(id=exp_id,
                                    program=program,
                                    host_id=host_id,
                                    environment=environment,
                                    duration_s=duration_s,
                                    sample_intervals=sample_intervals,
                                    total_energy=total_energy,
                                    tags=tag_objs)
            session.add(experiment)
        logger.info(f"Experiment {exp_id} inserted successfully.")
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to insert experiment {exp_id} : {e}")


def get_hosts_ids() -> List[str]:
    """
    Retrieve a list of host system IDs from the database.

    Returns:
        List[str]: A list of host system IDs.

    Raises:
        DatabaseException: If there is an error while retrieving the host IDs.
    """
    try:
        with Session() as session:
            hosts_ids = session.scalars(select(HostSystem.id)).all()
            return hosts_ids
    except SQLAlchemyError as e:
        raise DatabaseException(f"Getting hosts IDs failed: {e}")


def get_filtered_solutions(host: Optional[str] = None,
                           system_type: Optional[str] = None,
                           solver_method: Optional[str] = None,
                           tag_key: Optional[str] = None,
                           tag_value: Optional[str] = None) -> List[str]:
    """
    Fetch solutions based on optional filter criteria.

    Args:
        host (str, optional): The ID of the host to filter by.
        system_type (str, optional): The type of linear system to filter by.
        solver_method (str, optional): The solution method to filter by.
        tag_key (str, optional): The key of the tag to filter by.
        tag_value (str, optional): The value of the tag to filter by.

    Returns:
        list: A list of solution IDs that match the filters.

    Raises:
        DatabaseException: If the query fails.
    """
    try:
        with Session() as session:
            query = select(Solution.id).join(
                LinearSystem).join(Experiment).join(HostSystem).join(ExperimentTag).join(Tag)
            filters = []
            if host:
                filters.append(HostSystem.id == host)
            if system_type:
                filters.append(LinearSystem.system_type == system_type)
            if solver_method:
                filters.append(Solution.method == solver_method)
            if tag_key:
                filters.append(Tag.key == tag_key)
            if tag_value:
                filters.append(Tag.value == tag_value)
            if filters:
                query = query.filter(*filters)
            return session.scalars(query).all()
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to query solutions with filters: host={host}, "
            f"system_type={system_type}, solver_method={solver_method}, "
            f"tag={tag_key}:{tag_value}: {e}"
        )


def get_filtered_linear_systems(host: Optional[str] = None,
                                system_type: Optional[str] = None,
                                tag_key: Optional[str] = None) -> List[str]:
    """
    Retrieve a list of linear system IDs filtered by the specified criteria.

    Args:
        host (Optional[str]): The ID of the host system to filter by.
        system_type (Optional[str]): The type of the linear system to filter by (CPU or GPU).
        tag_key (Optional[str]): The key of the tag to filter by.

    Returns:
        List[str]: A list of linear system IDs that match the specified filters.
    """
    with Session() as session:
        query = select(LinearSystem.id).join(Experiment).join(
            HostSystem).join(ExperimentTag).join(Tag)
        filters = []
        if host:
            filters.append(HostSystem.id == host)
        if system_type:
            filters.append(LinearSystem.system_type == system_type)
        if tag_key:
            filters.append(Tag.key == tag_key)
        if filters:
            query = query.filter(*filters)
        return session.scalars(query).all()


def insert_system(id: str,
                  experiment_id: str,
                  system_name: str,
                  system_type: str,
                  system_data: dict,
                  core_count: dict,
                  perc_filtered: float,
                  tasks_data: dict) -> None:
    """
    Inserts a new linear system into the database.

    Args:
        id (str): The unique identifier for the linear system.
        experiment_id (str): The identifier for the associated experiment.
        system_name (str): The name of the linear system.
        system_type (str): The type of the linear system.
        system_data (dict): A dictionary containing the system equations.
        core_count (dict): A dictionary containing core count information.
        perc_filtered (float): Minimum percentage duration required for a task to be considered.
        tasks_data (dict): A dictionary containing tasks-related data.

    Raises:
        DatabaseException: If the linear system already exists or if there is an error during insertion.
    """
    try:
        with Session() as session, session.begin():
            if session.scalar(select(LinearSystem).filter_by(id=id)):
                raise DatabaseException(
                    f"Linear system {id} already exists.")
            new_system = LinearSystem(
                id=id,
                experiment_id=experiment_id,
                system_name=system_name,
                system_type=system_type,
                system_data=system_data,
                core_count=core_count,
                perc_filtered=perc_filtered,
                tasks_data=tasks_data
            )
            session.add(new_system)
        logger.info(f"Linear system {id} inserted successfully.")
    except SQLAlchemyError as e:
        raise DatabaseException(f"Failed to insert linear system {id}: {e}")


def insert_solution(id: str,
                    linear_system_id: str,
                    method: str,
                    solution_data: dict,
                    residuals: dict,
                    metrics: dict) -> None:
    """
    Inserts a new solution into the database.

    Args:
        id (str): The unique identifier for the solution.
        linear_system_id (str): The identifier for the associated linear system.
        method (str): The method used to obtain the solution.
        solution_data (dict): The data representing the solution.
        residuals (dict): The residuals associated with the solution.
        metrics (dict): The performance metrics of the solution.

    Raises:
        DatabaseException: If the solution already exists or if there is an error during insertion.
    """
    try:
        with Session() as session, session.begin():
            if session.scalar(select(Solution).filter_by(id=id)):
                raise DatabaseException(f"Solution {id} already exists.")
            new_solution = Solution(
                id=id,
                linear_system_id=linear_system_id,
                method=method,
                solution_data=solution_data,
                residuals=residuals,
                metrics=metrics
            )
            session.add(new_solution)
        logger.info(f"Solution {id} inserted successfully.")
    except SQLAlchemyError as e:
        raise DatabaseException(f"Failed to insert solution {id}: {e}")


def get_sampling_period(system_id: str) -> int:
    """
    Retrieve the sampling period for a given system based on its ID.

    Args:
        system_id (str): The ID of the system for which to retrieve the sampling period.

    Returns:
        int: The sampling period in milliseconds.

    Raises:
        DatabaseException: If the system does not exist or if there is an error during the query.
    """
    try:
        with Session() as session:
            system = session.scalar(select(LinearSystem)
                                    .filter_by(id=system_id))
            if not system:
                raise DatabaseException(
                    f"Linear system {system_id} does not exist.")
            sample_intervals = system.experiment.sample_intervals
            if "PKG" in system_id:
                return sample_intervals["RAPL_SAMPLE_INTERVAL_MS"]
            else:
                return sample_intervals["GPU_SAMPLE_INTERVAL_MS"]
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to get sampling period for system {system_id}: {e}")


def get_system(system_id: str) -> LinearSystem:
    """
    Retrieve a LinearSystem from the database by its ID.

    Args:
        system_id (str): The ID of the LinearSystem to retrieve.

    Returns:
        LinearSystem: The LinearSystem object corresponding to the given ID.

    Raises:
        DatabaseException: If the LinearSystem does not exist or if there is an error during retrieval.
    """
    try:
        with Session() as session:
            system = session.scalar(select(LinearSystem)
                                    .filter_by(id=system_id))
            if not system:
                raise DatabaseException(
                    f"Linear system {system_id} does not exist.")
            return system
    except SQLAlchemyError as e:
        raise DatabaseException(f"Failed to get system {system_id}: {e}")


def get_experiments(include_cluster: bool = False,
                    include_tags: bool = True) -> List[Experiment]:
    """
    Fetches all experiments from the database.

    Args:
        include_cluster (bool): Whether to include cluster details in the fetched experiments.
        include_tags (bool): Whether to include tags associated with experiments.

    Returns:
        List[Experiment]: A list of all Experiment objects in the database.

    Raises:
        DatabaseException: If there is an error fetching the experiments from the database.
    """
    try:
        with Session() as session:
            query = select(Experiment)
            if include_cluster:
                query = query.options(joinedload(Experiment.host)
                                      .joinedload(HostSystem.cluster))
            if include_tags:
                query = query.options(joinedload(Experiment.tags))
            experiments = session.scalars(query).unique().all()
            return experiments
    except SQLAlchemyError as e:
        raise DatabaseException(f"Failed to fetch all experiments: {e}")


def get_experiment_systems(experiment_id: str) -> List[LinearSystem]:
    """
    Fetch all linear systems associated with a given experiment ID from the database.

    Args:
        experiment_id (str): The ID of the experiment for which to fetch linear systems.

    Returns:
        List[LinearSystem]: A list of LinearSystem objects associated with the given experiment ID.

    Raises:
        DatabaseException: If there is an error fetching the systems from the database.
    """
    try:
        with Session() as session:
            systems = session.scalars(select(LinearSystem)
                                      .filter_by(experiment_id=experiment_id)).all()
            return systems
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to fetch all systems associated with experiment {experiment_id}: {e}")


def get_experiments_systems_ids(experiment_id: str) -> List[str]:
    """
    Retrieve the IDs of all linear systems associated with a given experiment.

    Args:
        experiment_id (str): The ID of the experiment for which to retrieve system IDs.

    Returns:
        List[str]: A list of system IDs associated with the specified experiment.

    Raises:
        DatabaseException: If there is an error accessing the database or retrieving the system IDs.
    """
    try:
        with Session() as session:
            systems_ids = session.scalars(select(LinearSystem.id)
                                          .filter_by(experiment_id=experiment_id)).all()
            return systems_ids
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to get systems IDs for experiment {experiment_id}: {e}")


def get_initial_system_solution(system_id: str) -> Solution:
    """
    Retrieve the initial solution for a given linear system from the database.

    Args:
        system_id (str): The unique identifier of the linear system.

    Returns:
        Solution: The initial solution associated with the specified linear system.

    Raises:
        DatabaseException: If the linear system does not exist or no solution is found.
        DatabaseException: If there is an error during the database query.
    """
    try:
        with Session() as session:
            system = session.scalar(
                select(LinearSystem).filter_by(id=system_id))
            if not system:
                raise DatabaseException(
                    f"Linear system {system_id} does not exist.")
            solution = system.solutions[0]
            if not solution:
                raise DatabaseException(
                    f"No solution found for linear system {system_id}.")
            return solution
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to get initial solution for system {system_id}: {e}")


def does_solution_exists(system_id: str, solver: str) -> bool:
    """
    Checks if a solution exists in the database for a given system and solver.

    Args:
        system_id (str): The identifier of the linear system.
        solver (str): The method or solver used to solve the linear system.

    Returns:
        bool: True if a solution exists, False otherwise.

    Raises:
        DatabaseException: If there is an error accessing the database.
    """
    try:
        with Session() as session:
            solution = session.scalar(
                select(Solution)
                .filter_by(linear_system_id=system_id, method=solver))
            return bool(solution)
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to check solution existence for system {system_id} and solver {solver}: {e}")


def does_experiment_exists(exp_id):
    """
    Check if an experiment exists in the database.

    Args:
        exp_id (int): The ID of the experiment to check.

    Returns:
        bool: True if the experiment exists, False otherwise.

    Raises:
        DatabaseException: If there is an error while checking the experiment's existence.
    """
    try:
        with Session() as session:
            experiment = session.scalar(
                select(Experiment)
                .filter_by(id=exp_id))
            return bool(experiment)
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to check experiment existence for {exp_id}: {e}")


def get_solutions_ids() -> List[str]:
    """
    Retrieve all solution IDs from the database.

    Returns:
        list: A list of solution IDs.

    Raises:
        DatabaseException: If there is an error retrieving the solution IDs from the database.
    """
    try:
        with Session() as session:
            solutions_ids = session.scalars(select(Solution.id)).all()
            return solutions_ids
    except SQLAlchemyError as e:
        raise DatabaseException(f"Failed to get solutions IDs: {e}")


def get_solution(solution_id: str) -> Solution:
    """
    Retrieve a solution from the database by its ID.

    Args:
        solution_id (str): The unique identifier of the solution to retrieve.

    Returns:
        Solution: The solution object corresponding to the given ID.

    Raises:
        DatabaseException: If the solution does not exist or if there is an error
                           during the retrieval process.
    """
    try:
        with Session() as session:
            solution = session.scalar(
                select(Solution)
                .filter_by(id=solution_id))
            if not solution:
                raise DatabaseException(
                    f"Solution {solution_id} does not exist.")
            return solution
    except Exception as e:
        raise DatabaseException(f"Failed to get solution {solution_id}: {e}")

# TODO read default solver from config file


def get_system_solution(system_id: str,
                        solver: str = "ols") -> Solution:
    """
    Retrieve the solution for a given linear system and solver from the database.

    Args:
        system_id (str): The identifier of the linear system.
        solver (str): The name of the solver used to solve the linear system.

    Returns:
        Solution: The solution object corresponding to the specified linear system and solver.

    Raises:
        DatabaseException: If the solution does not exist or if there is an error during retrieval.
    """
    try:
        with Session() as session:
            solution = session.scalar(
                select(Solution)
                .filter_by(linear_system_id=system_id, method=solver))
            if not solution:
                raise DatabaseException(
                    f"Solution for linear system {system_id} with solver {solver} does not exist.")
            return solution
    except Exception as e:
        raise DatabaseException(
            f"Failed to get solution for system {system_id} and solver {solver}: {e}")


def get_compatible_systems_ids(solution_id: str, include_self: bool = False) -> List[str]:
    """
    Retrieve a list of IDs of compatible linear systems for a given solution.

    Compatibility Criteria:
    1. The linear system belongs to the same host as the base system.
    2. The linear system is of the same type as the base system.
    3. (Optional) The linear system is not the same as the base system.
    4. The linear system has the same set of tasks as the base system.

    Args:
        solution_id (str): The ID of the solution for which to find compatible systems.
        include_self (bool, optional): Whether to include the base system in the list.

    Returns:
        List[str]: A list of IDs of compatible linear systems.

    Raises:
        DatabaseException: If the solution does not exist or a query fails.
    """
    try:
        with Session() as session:
            solution = session.scalar(
                select(Solution).filter_by(id=solution_id))
            if not solution:
                raise DatabaseException(
                    f"Solution {solution_id} does not exist.")
            system_base = solution.linear_system
            # First filtering by query
            filters = [
                Experiment.host_id == system_base.experiment.host_id,  # Same host
                LinearSystem.system_type == system_base.system_type   # Same system type
            ]
            if not include_self:
                # Optional: Not the same system
                filters.append(LinearSystem.id != system_base.id)
            systems = session.scalars(
                select(LinearSystem)
                .join(Experiment)
                .filter(*filters)
            ).all()
            # Second filtering by tasks
            tasks_base = set(system_base.tasks_data.keys())
            compatible_systems = [
                system for system in systems
                if set(system.tasks_data.keys()) == tasks_base
            ]
            return [system.id for system in compatible_systems]
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to get compatible systems for solution {solution_id}: {e}"
        )


def remove_experiment(experiment_id: str) -> None:
    """
    Remove an experiment from the database.

    Args:
        experiment_id (str): The ID of the experiment to be removed.

    Raises:
        DatabaseException: If the experiment does not exist or if there is an error
                           during the deletion process.
    """
    try:
        with Session() as session, session.begin():
            experiment = session.scalar(
                select(Experiment)
                .filter_by(id=experiment_id))
            if not experiment:
                raise DatabaseException(
                    f"Experiment {experiment_id} does not exist.")
            session.delete(experiment)
        logger.info(f"Experiment {experiment_id} removed successfully.")
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to remove experiment {experiment_id}: {e}")


def merge_databases(target_db_path: str, remove_after_merge: bool = False) -> None:
    """
    Merge another SQLite database into the current database, ignoring duplicates.

    Args:
        target_db_path (str): Path to the SQLite database to merge with the current database.
        remove_after_merge (bool, optional): Whether to remove the target database after merging.

    Raises:
        DatabaseException: If the database does not exist or the merge fails.
    """
    if not os.path.exists(target_db_path):
        raise DatabaseException(f"Database {target_db_path} does not exist.")
    engine_to_merge = create_engine(f'sqlite:///{target_db_path}')
    SessionToMerge = sessionmaker(bind=engine_to_merge)
    try:
        with SessionToMerge() as session_to_merge, Session() as session:
            merge_order = [
                Cluster,
                HostSystem,
                Tag,
                Experiment,
                LinearSystem,
                Solution
            ]
            tag_id_remap = {}
            experiment_insert_count = 0
            for model in merge_order:
                records_to_merge = session_to_merge.query(model).all()
                with session.begin():
                    for record in records_to_merge:
                        session_to_merge.expunge(record)
                        make_transient(record)

                        if model == Tag:
                            existing_tag = session.scalar(
                                select(Tag).filter_by(
                                    key=record.key,
                                    value=record.value
                                )
                            )
                            if existing_tag:
                                tag_id_remap[record.id] = existing_tag.id
                            else:
                                record.id = None
                                session.add(record)
                                session.flush()
                                tag_id_remap[record.id] = record.id

                        else:
                            existing_record = session.scalar(
                                select(model).filter_by(id=record.id)
                            )
                            if not existing_record:
                                session.add(record)
                                if model == Experiment:
                                    experiment_insert_count += 1
            experiment_tags_to_merge = session_to_merge.query(
                ExperimentTag).all()
            with session.begin():
                for record in experiment_tags_to_merge:
                    session_to_merge.expunge(record)
                    make_transient(record)
                    record.tag_id = tag_id_remap.get(
                        record.tag_id, record.tag_id)
                    existing_record = session.scalar(
                        select(ExperimentTag).filter_by(
                            experiment_id=record.experiment_id,
                            tag_id=record.tag_id
                        )
                    )
                    if not existing_record:
                        session.add(record)
        logger.info(f"Database {target_db_path} merged successfully, "
                    f"inserted {experiment_insert_count} new experiments.")
        if remove_after_merge:
            os.remove(target_db_path)
            logger.info(f"Database file {target_db_path} "
                        f"removed after merging.")
    except Exception as e:
        raise DatabaseException(
            f"Failed to merge database {target_db_path}: {e}"
        )


def get_clusters() -> List[Cluster]:
    """
    Retrieve all clusters from the database.

    Returns:
        List[Cluster]: A list of all Cluster objects in the database.

    Raises:
        DatabaseException: If there is an error fetching the clusters from the database.
    """
    try:
        with Session() as session:
            clusters = session.scalars(select(Cluster)).all()
            return clusters
    except SQLAlchemyError as e:
        raise DatabaseException(f"Failed to fetch all clusters: {e}")


def get_cluster(cluster_id: str) -> Cluster:
    """
    Retrieve a cluster from the database by its ID.

    Args:
        cluster_id (str): The unique identifier of the cluster to retrieve.

    Returns:
        Cluster: The Cluster object corresponding to the given ID.

    Raises:
        DatabaseException: If the cluster does not exist or if there is an error during retrieval.
    """
    try:
        with Session() as session:
            cluster = session.scalar(select(Cluster)
                                     .filter_by(id=cluster_id))
            if not cluster:
                raise DatabaseException(
                    f"Cluster {cluster_id} does not exist.")
            return cluster
    except SQLAlchemyError as e:
        raise DatabaseException(f"Failed to get cluster {cluster_id}: {e}")


def get_cluster_experiments(cluster_id: str) -> List[Experiment]:
    """
    Retrieve all experiments associated with a given cluster from the database.

    Args:
        cluster_id (str): The ID of the cluster for which to fetch experiments.

    Returns:
        List[Experiment]: A list of Experiment objects associated with the given cluster.

    Raises:
        DatabaseException: If there is an error fetching the experiments from the database.
    """
    try:
        with Session() as session:
            experiments = session.scalars(select(Experiment)
                                          .join(HostSystem)
                                          .filter(HostSystem.cluster_id == cluster_id)).all()
            return experiments
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to fetch all experiments associated with cluster {cluster_id}: {e}")


def get_solution_tags(solution_id: str) -> List[Tag]:
    """
    Retrieve all tags associated with a given solution from the database.

    Args:
        solution_id (str): The ID of the solution for which to fetch tags.

    Returns:
        List[Tag]: A list of Tag objects associated with the given solution.

    Raises:
        DatabaseException: If there is an error fetching the tags from the database.
    """
    try:
        with Session() as session:
            tags = session.scalars(select(Tag)
                                   .join(Experiment.tags)
                                   .join(Experiment.linear_systems)
                                   .join(LinearSystem.solutions)
                                   .filter(Solution.id == solution_id)).all()
            return tags
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to fetch tags associated with solution {solution_id}: {e}")


def get_linear_system_tags(system_id: str) -> List[Tag]:
    try:
        with Session() as session:
            tags = session.scalars(select(Tag)
                                   .join(Experiment.tags)
                                   .join(Experiment.linear_systems)
                                   .filter(LinearSystem.id == system_id)).all()
            return tags
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to fetch tags associated with system {system_id}: {e}")


def get_tags() -> List[Tag]:
    """
    Retrieve all tags from the database.

    Returns:
        List[Tag]: A list of all Tag objects in the database.

    Raises:
        DatabaseException: If there is an error fetching the tags from the database.
    """
    try:
        with Session() as session:
            tags = session.scalars(select(Tag)).all()
            return tags
    except SQLAlchemyError as e:
        raise DatabaseException(f"Failed to fetch all tags: {e}")


def get_tag_values(tag_key: str) -> List[str]:
    """
    Retrieve all values associated with a given tag key from the database.

    Args:
        tag_key (str): The key of the tag for which to fetch values.

    Returns:
        List[str]: A list of values associated with the given tag key.

    Raises:
        DatabaseException: If there is an error fetching the values from the database.
    """
    try:
        with Session() as session:
            values = session.scalars(select(Tag.value)
                                     .filter_by(key=tag_key)).all()
            return values
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to fetch values for tag key {tag_key}: {e}")


def get_systems_common_tasks(systems_ids: List[str]) -> List[str]:
    """
    Retrieve the common tasks among a list of linear systems.

    Args:
        systems_ids (List[str]): A list of linear system IDs for which to retrieve common tasks.

    Returns:
        List[str]: A list of tasks common to all linear systems in the given list.

    Raises:
        DatabaseException: If there is an error fetching the common tasks from the database.
    """
    try:
        with Session() as session:
            tasks = session.scalars(select(LinearSystem.tasks_data)
                                    .filter(LinearSystem.id.in_(systems_ids))).all()
            common_tasks = set(tasks[0].keys())
            for task in tasks[1:]:
                common_tasks &= set(task.keys())
            return list(common_tasks)
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to fetch common tasks for systems {systems_ids}: {e}")


def get_task_avg_duration(task: str, system_id: str) -> Optional[float]:
    """
    Retrieve the average duration of a task for a given system in milliseconds.

    Args:
        task (str): The name of the task for which to retrieve the average duration.
        system_id (str): The ID of the linear system for which to retrieve the average duration.

    Returns:
        Optional[float]: The average duration of the task in seconds, or None if not found.
        Some tasks may not have a duration associated with them (i.e Sleeping).

    Raises:
        DatabaseException: If there is an error fetching the average duration from the database.
    """
    try:
        with Session() as session:
            task_data = session.scalars(select(LinearSystem.tasks_data[task])
                                        .filter(LinearSystem.id == system_id)).all()
            duration: Optional[float] = task_data[0].get("AvgDuration")
            return duration
    except SQLAlchemyError as e:
        raise DatabaseException(
            f"Failed to fetch average duration for task {task} in system {system_id}: {e}")


def main():
    if len(sys.argv) < 2:
        logger.error(
            "Invalid number of arguments. Use 'create', 'remove', 'dump', 'reset' or 'merge <path>'.")
        exit(1)
    args = sys.argv[1:]
    if args[0] == "create":
        create_database()
    elif args[0] == "remove":
        remove_database()
    elif args[0] == "dump":
        dump_database(ENERGY_SOLVER_DB)
    elif args[0] == "reset":
        remove_database()
        create_database()
    elif args[0] == "merge":
        if len(args) < 2:
            logger.info(
                "Invalid number of arguments. Use 'merge <database_path>'.")
            return
        merge_databases(args[1])
    else:
        logger.error(
            "Invalid argument. Use 'create', 'remove', 'dump', 'reset' or 'merge <path>'.")
        exit(1)
    return


if __name__ == "__main__":
    main()
