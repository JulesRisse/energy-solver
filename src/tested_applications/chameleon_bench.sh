#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage: chameleon_bench <operation>"
    echo "see options in chameleon_dtesting -o help"
    return 1
fi
operation=$1

chameleon_dtesting_path=$(which chameleon_dtesting)
if [ -z "$chameleon_dtesting_path" ]; then
    echo "chameleon_dtesting not found in PATH"
    return 1
fi

cham_help_output=$(chameleon_dtesting -o help 2>/dev/null)
available_operations=$(echo "$cham_help_output" | awk '/The available operations are:/,/^$/' | tail -n +2 | awk '{print $1}' | tr '\n' ' ')

if ! echo "$available_operations" | grep -q "$operation"; then
    echo "ERROR : Operation '$operation' not found in available operations:"
    echo "$available_operations"
    return 1
else
    echo "Operation $operation found"
fi

env_var_or_default() {
    var_name=$1
    default_value=$2
    if [ -z "${!var_name+x}" ]; then
        echo "$default_value"
    else
        echo "${!var_name}"
    fi
}

mtxfmt=$(env_var_or_default "CHAMELEON_BENCH_MATRIX_FMT" "0")
bench_args=$(env_var_or_default "CHAMELEON_BENCH_ARGS" "")

# GPU exclusive or not
force_gpu=$(env_var_or_default "CHAMELEON_FORCE_GPU" 0)
if [ $force_gpu -eq 1 ]; then
    extras="--forcegpu $extras"
fi

# Number of cpus
cores=$(($(nproc --all) / 2 - 1))

# Number of gpus
detect_gpu() {
    local out="NONE:0"
    local has_gpu=0
    local num_nvidia=0
    local num_amd=0

    for device in /sys/class/drm/card*/device/vendor; do
        if [[ -f "$device" ]]; then
            local vendor_id
            vendor_id=$(cat "$device" 2>/dev/null)
            case "$vendor_id" in
            "0x10de")
                has_gpu=1
                num_nvidia=$((num_nvidia + 1))
                ;;
            "0x1002")
                has_gpu=1
                num_amd=$((num_amd + 1))
                ;;
            esac
        fi
    done

    if ((num_nvidia > 0 && num_amd > 0)); then
        out="CONFLICT:?"
    elif ((num_nvidia > 0)); then
        out="NVIDIA:$num_nvidia"
    elif ((num_amd > 0)); then
        out="AMD:$num_amd"
    fi
    echo "$out"
}

GPU_CONFIG=$(detect_gpu)
ngpus=$(echo $GPU_CONFIG | cut -d ':' -f 2)

cmd="$chameleon_dtesting_path \
-o $operation \
--mtxfmt $mtxfmt \
--gpus $ngpus \
--threads $cores \
--trace --human --nowarmup \
$bench_args"

echo "Running command :"
echo "$cmd"
eval "$cmd"
