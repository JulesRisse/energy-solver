#!/bin/bash

env_var_or_default() {
    var_name=$1
    default_value=$2
    if [ -z "${!var_name+x}" ]; then
        echo "$default_value"
    else
        echo "${!var_name}"
    fi
}

cores=$(($(nproc --all) / 2 - 1))
qrm_mb=$(env_var_or_default "QRM_MB" "256")

QRM_NCPU=$cores QRM_MB=$qrm_mb dqrm_sppotr -mat 7PT:30:40:50:1
