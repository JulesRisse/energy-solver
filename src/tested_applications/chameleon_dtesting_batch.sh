#!/bin/bash

# NOTE : when using "batch" operations, some variables have different meanings :
# dtrsm_batch Perform nb*ib triangular solve trsm( side, uplo, trns, diag, M, N, ... )
# dpotrf_batch Perform nb*ib Cholesky factorization potrf( uplo, N, ... )
# dsyrk_batch Perform nb*ib rank-k updates dsyrk( uplo, trans, N, K, ... )
# dgemm_batch Perform nb*ib general matrix-matrix multiply of size MxNxK

# THEREFORE :
# ib = number of operations per worker (and not inner tile size)
# nb = number of workers (and not tile size)

env_var_or_default() {
  var_name=$1
  default_value=$2
  if [ -z "${!var_name+x}" ]; then
    echo "$default_value"
  else
    echo "${!var_name}"
  fi
}

# Main parameter : operation to benchmark
operation=$(env_var_or_default "CHAMELEON_BATCH_OPERATION" "dgemm")

case "${operation}" in
dgemm | dtrsm | dpotrf | dsyrk) ;;
*)
  echo "Error: Invalid operation '$operation'."
  echo "Usage: $0 [dgemm|dtrsm|dpotrf|dsyrk]"
  return 1
  ;;
esac

# Other Chameleon parameters

# Matrix format (0: global, 1: tiles, 2: OOC)
mtxfmt=$(env_var_or_default "CHAMELEON_MATRIX_FMT" "0")

# Operation to benchmark
o="${operation}_batch"

# M N K dimensions
m=$(env_var_or_default "CHAMELEON_DIM_M" "1000")
n=$(env_var_or_default "CHAMELEON_DIM_N" "1000")
k=$(env_var_or_default "CHAMELEON_DIM_K" "1000")

# Number of iterations per worker
ib=$(env_var_or_default "CHAMELEON_BATCH_ITERATIONS" "100")

# Extra parameters ("--nowarmup --trace --human")
extras=$(env_var_or_default "CHAMELEON_BATCH_EXTRA_PARAMS" "--trace --human")

# GPU exclusive or not
force_gpu=$(env_var_or_default "CHAMELEON_FORCE_GPU" 0)
if [ $force_gpu -eq 1 ]; then
  extras="--forcegpu $extras"
fi

# Number of cpus
cores=$(($(nproc --all) / 2 - 1))

# Number of gpus
detect_gpu() {
  local out="NONE:0"
  local has_gpu=0
  local num_nvidia=0
  local num_amd=0

  for device in /sys/class/drm/card*/device/vendor; do
    if [[ -f "$device" ]]; then
      local vendor_id
      vendor_id=$(cat "$device" 2>/dev/null)
      case "$vendor_id" in
      "0x10de")
        has_gpu=1
        num_nvidia=$((num_nvidia + 1))
        ;;
      "0x1002")
        has_gpu=1
        num_amd=$((num_amd + 1))
        ;;
      esac
    fi
  done

  if ((num_nvidia > 0 && num_amd > 0)); then
    out="CONFLICT:?"
  elif ((num_nvidia > 0)); then
    out="NVIDIA:$num_nvidia"
  elif ((num_amd > 0)); then
    out="AMD:$num_amd"
  fi
  echo "$out"
}

GPU_CONFIG=$(detect_gpu)
ngpus=$(echo $GPU_CONFIG | cut -d ':' -f 2)

# Number of workers
if [ $force_gpu -eq 1 ]; then
  nb_workers=$ngpus
else
  nb_workers=$cores
fi

cmd="chameleon_dtesting \
--mtxfmt $mtxfmt \
-o $o \
--m $m \
--n $n \
--k $k \
--ib $ib \
--nb $nb_workers \
--gpus $ngpus \
 $extras"

echo "Running command :"
echo "$cmd"
eval "$cmd"
