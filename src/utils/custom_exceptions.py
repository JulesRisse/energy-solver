import traceback


class CustomException(Exception):
    def __init__(self, message):
        self.message = message
        tb = traceback.extract_stack(limit=2)[0]
        self.filename = tb.filename
        self.lineno = tb.lineno
        super().__init__(message)

    def __str__(self):
        return f'{self.__class__.__name__}: {self.message} (File "{self.filename}", line {self.lineno})'


class SolverException(CustomException):
    pass


class BuilderException(CustomException):
    pass


class DatabaseException(CustomException):
    pass


class PlottingException(CustomException):
    pass
