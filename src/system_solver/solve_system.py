import os
import sys
import numpy as np
import json
from scipy.optimize import lsq_linear
import statsmodels.api as sm
from scipy.sparse import csr_matrix
import logging
import datetime
from enum import Enum

# Add the parent directory to the path
FILEDIR = os.path.dirname(os.path.abspath(__file__))
SRC_DIR = os.path.abspath(os.path.join(FILEDIR, ".."))
sys.path.append(SRC_DIR)

# Import custom modules
import database.database_connector as dbc  # NOQA
from utils.custom_exceptions import DatabaseException, SolverException  # NOQA

# Logging basic configuration
logger = logging.getLogger('system_solver_logger')
logger.setLevel(logging.INFO)
console_handler = logging.StreamHandler(sys.stdout)
console_handler.setLevel(logging.INFO)
logger.addHandler(console_handler)

# Global constants
ROUND_VALUE = 6
WORKDIR = ''

# Add a constant to the system matrix (column depending on interval duration)
ADD_CONSTANT = False
# Add an intercept to the OLS model (column of 1's in the system matrix)
ADD_OLS_INTERCEPT = False
# Show matrices in the logs
SHOW_MATRICES = False
# Use sparse matrices solvers
SPARSE_SOLVER = False
# Elastic Net regularization parameters
ELASTIC_NET_ALPHA = 0.01
ELASTIC_NET_RATIO = 0.5

# Enum for the solver type


class SolverType(Enum):
    LSQ_LINEAR = "lsqlinear"
    OLS = "ols"
    GLM = "glm"


# Environment variable
ENERGY_SOLVER_METHOD = os.environ.get(
    'ENERGY_SOLVER_METHOD', SolverType.OLS.value)
LSQ_LINEAR_UPPER_PKG = os.environ.get('LSQ_LINEAR_UPPER_PKG', 'inf')
LSQ_LINEAR_UPPER_GPU = os.environ.get('LSQ_LINEAR_UPPER_GPU', 'inf')


class Result:
    def __init__(self,
                 exp_id,
                 system_type,
                 solver_type,
                 solution,
                 residuals,
                 metrics):
        self.exp_id = exp_id
        self.system_type = system_type
        self.solver_type = solver_type
        self.id = f"{exp_id}_{system_type}_{solver_type}"
        self.solution = solution
        self.residuals = residuals
        self.metrics = metrics

    def save_to_database(self):
        linear_system_id = f"{self.exp_id}_{self.system_type}"
        solution_data = self.solution
        residuals = self.residuals.tolist()
        try:
            dbc.insert_solution(self.id,
                                linear_system_id,
                                self.solver_type,
                                solution_data,
                                residuals,
                                self.metrics)
        except DatabaseException as e:
            raise e

    def save_to_file(self):
        filename = os.path.join(WORKDIR, f'solution_{self.id}.json')
        with open(filename, 'w') as json_file:
            json.dump(self.solution, json_file, indent=4)

    def __str__(self):
        return f"Solution for {self.id}"


def configure_logger():
    """
    Configures the logger for the solver application.

    This function sets up the logger to log messages to both the console and a log file.
    It creates a log file with a timestamped name in the specified output directory.

    Returns:
        None
    """
    if WORKDIR == '':
        logger.error(
            'Please set the output directory before configuring the logger.')
        sys.exit(1)
    now = datetime.datetime.now()
    log_path = WORKDIR
    if not os.path.exists(log_path):
        os.makedirs(log_path)
    logfile_name = now.strftime("%Y-%m-%d_%H-%M-%S") + '_solver.log'
    logfile_path = os.path.join(log_path, logfile_name)
    file_handler = logging.FileHandler(logfile_path, 'w')
    file_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s\n%(message)s')
    console_handler.setFormatter(formatter)
    file_handler.setFormatter(formatter)
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)


def load_json_system_from_file(experiment_path):
    """
    Loads the system JSON data from the JSON file in the experiment folder.

    Args:
        experiment_path (str): The path to the experiment folder.

    Returns:
        dict: The system data loaded from the JSON file.
    """
    json_path = os.path.join(
        experiment_path, 'builder_results', 'system.json')
    if not os.path.exists(json_path):
        logger.error(
            'The system JSON file does not exist. Did you run the system builder?')
        return None
    with open(json_path, 'r') as json_file:
        system = json.load(json_file)
    return system


def create_system_matrices(results):
    """
    Generate system matrices for each system type based on the given results, and yield them.

    Args:
        results (dict): The linear systems dictionnary representation
    Returns:
        dictionary: A dictionary containing the system matrices for each system type :
            - Key : system type
            - Value : tuple (A, b, dict_vars) where A is the system matrix, b the system vector, and dict_vars the mapping of variable names to their corresponding index in the system matrix.
    """
    dict_results = {}
    for system_type in results.keys():
        result = results[system_type]
        dict_results[system_type] = create_system_matrix(result, system_type)
    return dict_results


def create_system_matrix(result, system_type):
    global ADD_CONSTANT
    global SPARSE_SOLVER
    if ADD_CONSTANT:
        dict_vars = {'StaticPower': 0}
        idx = 1
    else:
        dict_vars = {}
        idx = 0
    # Construct the x vector indexes
    for interval_id in result.keys():
        for var in result[interval_id]['TasksTotalTime'].keys():
            if var not in dict_vars:
                dict_vars[var] = idx
                idx += 1
    M = len(result.keys())
    N = len(dict_vars)
    A = np.zeros((M, N))
    b = np.zeros(M)
    # Iterate over intervals (equations i.e., lines)
    for i, interval_id in enumerate(result.keys()):
        # Constant  = package static power
        if ADD_CONSTANT:
            A[i, 0] = 1 * result[interval_id]['Duration'] / 1000
        # Iterate over variables (columns) to set A
        for var in result[interval_id]['TasksTotalTime'].keys():
            task_time = result[interval_id]['TasksTotalTime'][var]
            # Convert to seconds
            A[i, dict_vars[var]] = task_time / 1000
        # Set B
        b[i] = result[interval_id]['Energy']
    sparse_A = csr_matrix(A)
    nnz = sparse_A.nnz
    density = nnz / (M * N)
    logger.info(
        f'System {system_type} : {M} equations, {N} variables, {nnz} non-zero values, density {density}')
    if SPARSE_SOLVER:
        tuple_result = (sparse_A, b, dict_vars)
    else:
        tuple_result = (A, b, dict_vars)
    return tuple_result


def solve_system_lsq_linear(A, b, dict_vars, system_type, exp_id):
    """
    Solve the system of linear equations given by A, b, and dict_vars.
    Uses scipy.optimize.lsq_linear (Least Squares with bounds)
    Creates a Result object containing the solution, residuals, and metrics.

    Args:
        A (numpy.ndarray): The system matrix.
        b (numpy.ndarray): The system vector.
        dict_vars (dict): A dictionary mapping variable names to their corresponding index in the system matrix.
        system_type (str): The type of system (of the form PKG_i or GPU_i ).
        df_trace (pandas.DataFrame): The DataFrame containing the trace data.

    Returns:
        numpy.ndarray: The solution vector x.
    """
    # Bounds
    bound_inf = 0
    if system_type.startswith('PKG'):
        bound_sup = LSQ_LINEAR_UPPER_PKG
        if bound_sup == 'inf':
            bound_sup = np.inf
        else:
            bound_sup = int(bound_sup)
    else:
        bound_sup = LSQ_LINEAR_UPPER_GPU
        if bound_sup == 'inf':
            bound_sup = np.inf
        else:
            bound_sup = int(bound_sup)
    # Solve the system
    res = lsq_linear(A, b, bounds=(bound_inf, bound_sup), verbose=0)
    x = res.x
    # Print the solution
    sol_str = 'Solution for system ' + system_type + ' : \n'
    dict_solution = {}
    for var, idx in dict_vars.items():
        value = x[idx]
        dict_solution[var] = value
        sol_str += f'\t {var} : {value} W \n'
    # Compute conditional number
    metrics_str = 'RELEVANT METRICS \n'
    cond = np.linalg.cond(A)
    metrics_str += f'Condition Nb A : {cond} \n'
    # Compute predicted values
    predicted = A @ x
    # Compute residuals
    residuals = b - predicted
    # Compute RMSD
    rmsd = np.sqrt(np.mean(residuals**2))
    metrics_str += f'RMSD: {rmsd} \n'
    # Compute Mean Absolute Percentage Error (MAPE)
    mape = np.mean(np.abs(residuals / b)) * 100
    metrics_str += f'MAPE: {mape} % \n'
    # Compute R squared
    ss_total = np.sum((b - np.mean(b))**2)
    ss_residual = np.sum(residuals**2)
    r_squared = 1 - (ss_residual / ss_total)
    metrics_str += f'R-squared: {r_squared} \n'
    # Compute residuals norm
    norm_b = np.linalg.norm(b)
    norm_residuals = np.linalg.norm(residuals)
    percent_residuals = (norm_residuals / norm_b) * 100
    metrics_str += (f'Residuals norm: {round(norm_residuals, 2)} '
                    f'({round(percent_residuals, 2)} % of b norm) \n')
    # Write it in a summary html
    metrics_html = '<table class="table"><tbody>'
    metrics_html += (f'<tr><th scope="row">Condition Nb A: '
                     f'</th><td>{cond}</td></tr>')
    metrics_html += f'<tr><th scope="row">RMSD:</th><td>{rmsd}</td></tr>'
    metrics_html += f'<tr><th scope="row">MAPE:</th><td>{mape} %</td></tr>'
    metrics_html += (f'<tr><th scope="row">R-squared: '
                     f'</th><td>{r_squared}</td></tr>')
    metrics_html += (f'<tr><th scope="row">Residuals norm: '
                     f'</th><td>{round(norm_residuals, 2)} '
                     f'({round(percent_residuals, 2)} % of b norm)</td></tr>')
    metrics_html += '</tbody></table>'
    # Log the results
    logger.info(sol_str)
    logger.info(metrics_str)
    # Create result object
    solver_type = SolverType.LSQ_LINEAR.value
    result = Result(exp_id, system_type, solver_type,
                    dict_solution, residuals, metrics_html)
    return result


def solve_system_statsmodel_ols(A, b, dict_vars, system_type, exp_id):
    """
    Solve the system of linear equations given by A, b, and dict_vars.
    Uses statsmodels.OLS (Ordinary Least Squares).
    Creates a Result object containing the solution, residuals, and metrics.

    Args:
        A (numpy.ndarray): The system matrix.
        b (numpy.ndarray): The system vector.
        dict_vars (dict): A dictionary mapping variable names to their corresponding index in the system matrix.
        system_type (str): The type of system (of the form PKG_i or GPU_i ).
        exp_id (str): The experiment ID.

    Returns:
        Result: The result object containing the solution, residuals, and metrics.
    """
    # Handle intercept
    global ADD_OLS_INTERCEPT
    if ADD_OLS_INTERCEPT:
        logger.info('Adding intercept')
        A = sm.add_constant(A)
    # Fit the model
    model = sm.OLS(b, A)
    results = model.fit()
    # Compute validation metrics
    logger.info(results.summary().as_text())
    # Mean Squared Residual Error (MSRE)
    MSRE = results.mse_resid
    # Solution vector and residuals
    x = results.params
    residuals = results.resid
    # Solution logging
    sol_str = 'Solution for system ' + system_type + ' : \n'
    dict_solution = {}
    for var, idx in dict_vars.items():
        value = x[idx]
        dict_solution[var] = value
        sol_str += f'\t {var} : {value} W \n'
    logger.info(sol_str)
    # Root Mean Square Deviation (RMSD)
    rmsd = np.sqrt(np.mean(residuals**2))
    # Mean Absolute Percentage Error (MAPE)
    mape = np.mean(np.abs(residuals / b)) * 100
    # Log RMSD and MAPE
    logger.info(f'RMSD: {rmsd}')
    logger.info(f'MAPE: {mape:.2f} %')
    # Summary HTML modification to include MSRE, RMSD, and MAPE
    summary_html = results.summary().as_html()
    msre_index = summary_html.find('<caption>OLS Regression Results</caption>')
    msre_index += len('<caption>OLS Regression Results</caption>')
    msre_html = f'<tr><th>MSRE:</th><td>{MSRE}</td></tr>'
    rmsd_html = f'<tr><th>RMSD:</th><td>{rmsd}</td></tr>'
    mape_html = f'<tr><th>MAPE:</th><td>{mape:.2f} %</td></tr>'
    summary_html = summary_html[:msre_index] + msre_html + \
        rmsd_html + mape_html + summary_html[msre_index:]
    # Create result object
    solver_type = SolverType.OLS.value
    result = Result(exp_id, system_type, solver_type,
                    dict_solution, residuals, summary_html)
    return result


def solve_system_statsmodel_glm(A, b, dict_vars, system_type, exp_id):
    """
    Solve the system of linear equations given by A, b, and dict_vars
    Uses statsmodels.GLM (Generalized Linear Models)
    Creates a Result object containing the solution, residuals, and metrics.

    Args:
        A (numpy.ndarray): The system matrix.
        b (numpy.ndarray): The system vector.
        dict_vars (dict): A dictionary mapping variable names to their corresponding index in the system matrix.
        system_name (str): The name of the system (of the form PKG_i or GPU_i ).
        suffix (str): A suffix to append to the system name.

    Returns:
        numpy.ndarray: The solution vector x.
    """
    # Handle intercept
    if ADD_OLS_INTERCEPT:
        logger.info('Adding intercept')
        A = sm.add_constant(A)
    # Fit the model
    family = sm.families.Gaussian()
    model = sm.GLM(b, A, family=family)
    results_reg = model.fit_regularized(
        alpha=ELASTIC_NET_ALPHA, L1_wt=ELASTIC_NET_RATIO)
    x = results_reg.params
    # Compute conditional number
    metrics_str = 'RELEVANT METRICS \n'
    cond = np.linalg.cond(A)
    metrics_str += f'Condition Nb A : {cond} \n'
    # Compute predicted values
    predicted = A @ x
    # Compute residuals
    residuals = b - predicted
    # Compute RMSD
    rmsd = np.sqrt(np.mean(residuals**2))
    metrics_str += f'RMSD: {rmsd} \n'
    # Compute MAPE
    mape = np.mean(np.abs(residuals / b)) * 100
    metrics_str += f'MAPE: {mape} % \n'
    # Compute adjusted R squared
    ss_total = np.sum((b - np.mean(b))**2)
    ss_residual = np.sum(residuals**2)
    r_squared = 1 - (ss_residual / ss_total)
    n = len(b)
    p = len(x)
    r_squared_adj = 1 - ((1 - r_squared) * (n - 1) / (n - p - 1))
    metrics_str += f'Adjusted R-squared: {r_squared_adj:.3f} \n'
    # Compute residuals norm
    norm_b = np.linalg.norm(b)
    norm_residuals = np.linalg.norm(residuals)
    percent_residuals = (norm_residuals / norm_b) * 100
    metrics_str += (f'Residuals norm: {round(norm_residuals, 2)} '
                    f'({round(percent_residuals, 2)} % of b norm) \n')
    # Write it in a summary html
    metrics_html = (f'<table><caption>Metrics for system '
                    f'{system_type}</caption>')
    metrics_html += f'<tr><th>Condition Nb A:</th><td>{cond}</td></tr>'
    metrics_html += f'<tr><th>RMSD:</th><td>{rmsd}</td></tr>'
    metrics_html += f'<tr><th>MAPE:</th><td>{mape} %</td></tr>'
    metrics_html += f'<tr><th>R-squared:</th><td>{r_squared}</td></tr>'
    metrics_html += (f'<tr><th>Residuals norm:</th><td>{round(norm_residuals, 2)} '
                     f'({round(percent_residuals, 2)} % of b norm)</td></tr>')
    metrics_html += '</table>'
    sol_str = 'Solution for system ' + system_type + ' : \n'
    dict_solution = {}
    for var, idx in dict_vars.items():
        value = x[idx]
        dict_solution[var] = value
        sol_str += f'\t {var} : {value} W \n'
    logger.info(sol_str)
    # Create result object
    solver_type = SolverType.GLM.value
    result = Result(exp_id, system_type, solver_type,
                    dict_solution, residuals, metrics_html)
    return result


def compare_solution_and_system(solution_id, system_id):
    solution = dbc.get_solution(solution_id)
    solution_data = solution.solution_data
    system = dbc.get_system(system_id)
    system_data = system.system_data
    # Build the system matrix
    A, b, dict_vars = create_system_matrix(system_data, system.system_name)
    # Build the solution vector
    X = np.zeros(len(dict_vars))
    for var, idx in dict_vars.items():
        X[idx] = solution_data[var]
    # Compute the predicted values
    predicted = A @ X
    # Compute the residuals
    residuals = b - predicted
    # Compute the RMSD
    rmsd = np.sqrt(np.mean(residuals**2))
    # Compute MAPE
    mape = np.mean(np.abs(residuals / b)) * 100
    # Compute the R squared
    ss_total = np.sum((b - np.mean(b))**2)
    ss_residual = np.sum(residuals**2)
    r_squared = 1 - (ss_residual / ss_total)
    # Compute the residuals norm
    norm_b = np.linalg.norm(b)
    norm_residuals = np.linalg.norm(residuals)
    perc_residuals = (norm_residuals / norm_b) * 100
    dict_metrics = {'rmsd': rmsd,
                    'r_squared': r_squared,
                    'norm_residuals': norm_residuals,
                    'perc_residuals': perc_residuals,
                    'mape': mape}
    return dict_metrics


def solve_system(A: np.ndarray,
                 b: np.ndarray,
                 dict_vars: dict,
                 system_type: str,
                 exp_id: str,
                 solver: str) -> Result:
    # Print the system
    global SHOW_MATRICES
    if (SHOW_MATRICES):
        logger.info(f'\n System A x = b for {system_type} \n'
                    + f'\t Matrix A : {A} \n'
                    + f'\t Matrix b : {b} \n'
                    + f'\t dict_vars : {dict_vars} \n')
    else:
        logger.info(f'\n System A x = b for {system_type} \n'
                    + f'\t dict_vars : {dict_vars} \n')
    # Solve the system
    # Environment variable choses the solver method
    if solver == SolverType.LSQ_LINEAR.value:
        result = solve_system_lsq_linear(A, b, dict_vars, system_type, exp_id)
    elif solver == SolverType.OLS.value:
        result = solve_system_statsmodel_ols(
            A, b, dict_vars, system_type, exp_id)
    elif solver == SolverType.GLM.value:
        result = solve_system_statsmodel_glm(
            A, b, dict_vars, system_type, exp_id)
    else:
        raise SolverException(
            f'Unknown solver method : {solver} for system {system_type}')
    return result


def solve_from_db(experiment_id, solver):
    """
    Entry point of the script for the web application.

    This script should be called after building the system JSON data.
    Takes the experience id as an argument
    and performs the following steps:

    1. Load the system JSON data from the database.
    2. Create the system matrix and the target vector.
    3. Solve the system using the specified method.
    4. Save the results to a JSON file & in the database.

    Args:
        experiment_id (str): The id of the experiment in the database.
        solver (str): The solver method to use (lsqlinear, ols, glm).

    Returns:
        None
    """
    # Set the input directory
    try:
        systems = dbc.get_experiment_systems(experiment_id)
        if len(systems) == 0:
            raise SolverException(
                f"Error: No systems found for experiment {experiment_id}")
    except DatabaseException as e:
        raise SolverException(f"Database error: {e}")

    # Load the system JSON data
    systems_dict = {}
    for system in systems:
        try:
            system_name = system.system_name
            system_data = system.system_data
            systems_dict[system_name] = system_data
        except DatabaseException as e:
            raise SolverException(
                f"Error in solver (loading system json) : {e}")

    # Create the system matrix and the target vector
    dic_matrixes = create_system_matrices(systems_dict)

    # Solve the systems
    for system_type in dic_matrixes.keys():
        logger.info(f"Solving system {system_type} with solver {solver}")
        A, b, dict_vars = dic_matrixes[system_type]
        result: Result = solve_system(
            A, b, dict_vars, system_type, experiment_id, solver)
        try:
            result.save_to_database()
        except DatabaseException as e:
            raise SolverException(
                f"Failed to save solution result for system {system_type} "
                f"and solver {solver} in the database: {e}")


def main():
    """
    Entry point of the script.

    This script should be called after building the system JSON data.
    Takes the experience folder as a command-line argument 
    and performs the following steps:

    1. Load the system JSON data.
    2. Create the system matrix and the target vector.
    3. Solve the system using the specified method.
    4. Save the results in the database.

    Args:
        None

    Returns:
        None
    """
    # Arg is the experience directory
    if (len(sys.argv) < 2):
        print('Usage: python solve_system.py <exp_directory>')
        sys.exit(1)

    # Set the input directory
    exp_directory = sys.argv[1]

    # get the experiment id which is the last part of the path
    exp_directory = os.path.normpath(exp_directory)
    exp_id = os.path.basename(exp_directory)
    try:
        do_exists = dbc.does_experiment_exists(exp_id)
        if do_exists == False:
            logger.error(
                f"Experiment {exp_id} does not exist in the database.")
            sys.exit(1)
    except DatabaseException as e:
        logger.error(f"Error in solver : {e}")
        sys.exit(1)

    if not os.path.isdir(exp_directory):
        print('The directory', exp_directory, 'does not exist.')
        sys.exit(1)

    solvers_dir = os.path.join(exp_directory, 'solver_results')
    if not os.path.exists(solvers_dir):
        print('The directory', solvers_dir, 'does not exist.')
        sys.exit(1)
    global WORKDIR
    WORKDIR = solvers_dir

    # Set the output directory
    configure_logger()

    # Load the system JSON data
    system_dict = load_json_system_from_file(exp_directory)
    if system_dict is None:
        sys.exit(1)

    # Create the system matrix and the target vector
    dic_matrixes = create_system_matrices(system_dict)

    # Solve the systems
    for system_type in dic_matrixes.keys():
        A, b, dict_vars = dic_matrixes[system_type]
        result: Result = solve_system(A, b, dict_vars, system_type,
                                      exp_id, ENERGY_SOLVER_METHOD)
        try:
            result.save_to_database()
        except DatabaseException as e:
            logger.error(f"Error while saving the result in the database: {e}")
            sys.exit(1)
        try:
            result.save_to_file()
        except Exception as e:
            logger.error(f"Error while saving the result in a file: {e}")
            sys.exit(1)


if __name__ == '__main__':
    main()
