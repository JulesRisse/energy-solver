#!/usr/bin/python3
from collections import defaultdict
from bokeh.plotting import figure, show as bokeh_show
from bokeh.models import ColumnDataSource, HoverTool, FactorRange, Span, Column, Whisker
from bokeh.models import CustomJSTickFormatter
from bokeh.models import Button, CustomJS, Legend, Label, Tabs, TabPanel
from bokeh.models import WheelZoomTool
from bokeh.transform import cumsum
from bokeh.embed import components
from bokeh.layouts import column, row
import scipy.stats as stats
from scipy.stats import sem
import numpy as np
from typing import Tuple, List, Dict, Any
import math
import os
import sys

# Add the parent directory to the path
FILEDIR = os.path.dirname(os.path.abspath(__file__))
SRC_DIR = os.path.abspath(os.path.join(FILEDIR, ".."))
sys.path.append(SRC_DIR)

# Import custom modules
import database.database_connector as dbc  # noqa
from utils.custom_exceptions import PlottingException, DatabaseException  # noqa

# Constants
TAB_WIDTH = 1450
TAB_HEIGHT = 450
BOX_SIZE = 400


def get_download_source_button(source: ColumnDataSource,
                               filename: str) -> Button:
    button = Button(label="Download CSV", button_type="success")
    button.js_on_click(CustomJS(args=dict(source=source), code="""
        const data = source.data;
        const columns = Object.keys(data);
        const csvRows = [];
        csvRows.push(columns.join(','));
        for (let i = 0; i < data[columns[0]].length; i++) {
            csvRows.push(columns.map(column => data[column][i]).join(','));
        }
        const blob = new Blob([csvRows.join('\\n')], { type: 'text/csv' });
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = '%s';
        a.click();
        URL.revokeObjectURL(url);
    """ % filename))
    return button


def prepare_data_times(system):
    num_intervals = len(system.keys())
    data_times = {'Intervals': [], 'Timestamps': []}
    idx = 0
    for interval in system.keys():
        timestamp = str(round(system[interval]['BeginTimestamp']))
        data_times['Timestamps'].append(timestamp)
        data_times['Intervals'].append(interval)
        for task in system[interval]['TasksTotalTime'].keys():
            # First time task is encountered
            if task not in data_times.keys():
                data_times[task] = [0] * num_intervals
            # Append its time to the interval index
            taks_time = system[interval]['TasksTotalTime'][task]
            data_times[task][idx] = taks_time
        idx += 1
    return data_times


def prepare_data_energy(system, solution):
    num_intervals = len(system.keys())
    data_energy_model = {'Intervals': [], 'Timestamps': []}
    data_energy_cumulated_real = {
        'Intervals': [], 'Timestamps': [], 'ActualEnergy': []}
    data_energy_cumulated_model = {
        'Intervals': [], 'Timestamps': [], 'ModelEnergy': []}
    idx = 0
    for interval in system.keys():
        timestamp = str(round(system[interval]['BeginTimestamp']))
        # Add real energy data
        data_energy_cumulated_real['Intervals'].append(interval)
        data_energy_cumulated_real['Timestamps'].append(timestamp)
        data_energy_cumulated_real['ActualEnergy'].append(
            system[interval]['Energy'])
        # Add model energy data
        data_energy_model['Intervals'].append(interval)
        data_energy_model['Timestamps'].append(timestamp)
        data_energy_cumulated_model['Intervals'].append(interval)
        data_energy_cumulated_model['Timestamps'].append(timestamp)
        cumulated_predicted_energy = 0
        for task in system[interval]['TasksTotalTime'].keys():
            # First time task is encountered
            if task not in data_energy_model.keys():
                data_energy_model[task] = [0] * num_intervals
            # Append its time to the interval index
            taks_time = system[interval]['TasksTotalTime'][task]
            if task not in solution:
                raise ValueError(f"Task '{task}' not found in solution")
            task_power_solution = solution[task]
            # Task time is in ms, convert to seconds
            task_time_seconds = taks_time / 1000
            task_energy_predicted = task_power_solution * task_time_seconds
            cumulated_predicted_energy += task_energy_predicted
            data_energy_model[task][idx] = task_energy_predicted
        idx += 1
        data_energy_cumulated_model['ModelEnergy'].append(
            cumulated_predicted_energy)
    return data_energy_model, data_energy_cumulated_model, data_energy_cumulated_real


def generate_bokeh_time_plot(system_name,
                             data_times,
                             sampling_period_ms,
                             core_count,
                             tasks_data):
    # Color palette
    tasks = tasks_data.keys()
    color_mapping = [tasks_data[task]['Color'] for task in tasks]

    # Plot for task durations
    source_times = ColumnDataSource(data=data_times)
    plot_title = " ".join([
        f"Task durations for system {system_name} per interval.",
        f"Sample period = {sampling_period_ms} ms"])
    p1 = figure(x_range=FactorRange(*data_times['Intervals']),
                title=plot_title,
                height=TAB_HEIGHT,
                width=TAB_WIDTH,
                toolbar_location="above")

    p1.grid.minor_grid_line_color = '#eeeeee'
    bar_renderer_time = p1.vbar_stack(stackers=tasks,
                                      x='Intervals',
                                      width=0.8,
                                      color=color_mapping,
                                      source=source_times)
    p1.yaxis.axis_label = "Time (ms)"
    p1.y_range.start = 0
    p1.xgrid.grid_line_color = None
    wheel_zoom_tool_width = WheelZoomTool(dimensions="width")
    p1.add_tools(wheel_zoom_tool_width)
    p1.toolbar.active_scroll = wheel_zoom_tool_width
    p1.xaxis.axis_label = "Timestamps"
    p1.title.text_font_size = '10pt'
    p1.xaxis.major_label_orientation = 45
    p1.xaxis.major_label_text_font_size = '7pt'
    theoretical_time = int(core_count) * int(sampling_period_ms)
    theoretical_time_span = Span(location=theoretical_time,
                                 dimension='width',
                                 line_color='green',
                                 line_dash='dashed',
                                 line_width=3)
    p1.add_layout(theoretical_time_span)
    theoretical_time_label = Label(x=0, y=theoretical_time,
                                   x_offset=10, y_offset=5,
                                   text=f'Theoretical Time: '
                                   f'{theoretical_time} ms',
                                   background_fill_color='white',
                                   text_font_size='11pt',
                                   text_color='green',
                                   background_fill_alpha=0.7)
    p1.add_layout(theoretical_time_label)

    # FuncTickFormatter for x-axis to show Timestamps
    intervals = data_times['Intervals']
    time_axis = data_times['Timestamps']
    tick_labels = {str(i): ts for i, ts in zip(intervals, time_axis)}
    p1.xaxis.formatter = CustomJSTickFormatter(code="""
        var labels = %s;
        return labels[tick.toString()];
    """ % tick_labels)

    # Add a legend
    legend_items = [(task, [renderer])
                    for task, renderer in zip(tasks, bar_renderer_time)]
    legend = Legend(items=legend_items, location="top_right",
                    click_policy="hide")
    p1.add_layout(legend)

    # Hover tooltips
    tooltips_vbar_time = [("Task", "$name"),  ("Time", "@$name"),
                          ("Timestamp", "@Timestamps"), ("Interval", "@Intervals")]
    bar_hover_time = HoverTool(
        tooltips=tooltips_vbar_time, renderers=bar_renderer_time)
    p1.add_tools(bar_hover_time)

    return p1


def generate_bokeh_energy_plot(system_id, solver,
                               data_energy_model, data_energy_cumulated_model,
                               data_energy_cumulated_real, tasks_data):
    # Color palette
    tasks = tasks_data.keys()
    color_mapping = [tasks_data[task]['Color'] for task in tasks]

    # generate column data sources
    source_energy_model = ColumnDataSource(data=data_energy_model,
                                           name=f"{system_id}_energy_model")
    source_energy_cumul_model = ColumnDataSource(data=data_energy_cumulated_model,
                                                 name=f"{system_id}_energy_cumul_model")
    source_energy_cumul_real = ColumnDataSource(data=data_energy_cumulated_real,
                                                name=f"{system_id}_energy_cumul_real")

    # Plot for model & real energy consumption
    figure_name = f"{system_id}_energy"
    p2 = figure(x_range=FactorRange(*data_energy_model['Intervals']),
                title=f"Estimated and real energy consumption ({solver})",
                height=TAB_HEIGHT,
                width=TAB_WIDTH,
                toolbar_location="below",
                name=figure_name)
    bar_renderers_energy = p2.vbar_stack(stackers=tasks,
                                         x='Intervals',
                                         width=0.8,
                                         color=color_mapping,
                                         source=source_energy_model)
    for renderer in bar_renderers_energy:
        renderer.tags.append("bar-renderer-energy-tag")
    p2.y_range.start = 0
    p2.yaxis.axis_label = "Energy (J)"
    p2.xaxis.axis_label = "Timestamps"
    p2.xgrid.grid_line_color = None

    # Line chart of total real energy consumption
    line_renderer_actual = p2.line(x='Intervals',
                                   y='ActualEnergy',
                                   source=source_energy_cumul_real,
                                   line_width=2,
                                   color="red",
                                   legend_label="Actual Energy")
    line_renderer_actual.tags.append("line-renderer-actual-tag")

    # Line chart of total model energy consumption
    line_renderer_model = p2.line(x='Intervals',
                                  y='ModelEnergy',
                                  source=source_energy_cumul_model,
                                  line_width=2,
                                  color="blue",
                                  legend_label="Modeled Energy")
    line_renderer_model.tags.append("line-renderer-model-tag")

    p2.xaxis.visible = False
    wheel_zoom_tool_width = WheelZoomTool(dimensions="width")
    p2.add_tools(wheel_zoom_tool_width)
    p2.toolbar.active_scroll = wheel_zoom_tool_width

    # Hover tooltips
    tooltips_vbar_energy = [("Task", "$name"), ("Energy", "@$name"),
                            ("Timestamp", "@Timestamps"), ("Interval", "@Intervals")]
    tooltips_line_actual = [("Actual Energy", "@ActualEnergy"),
                            ("Timestamp", "@Timestamps"), ("Interval", "@Intervals")]
    tooltips_line_model = [("Modeled Energy", "@ModelEnergy"),
                           ("Timestamp", "@Timestamps"), ("Interval", "@Intervals")]
    bar_hover_energy = HoverTool(tooltips=tooltips_vbar_energy,
                                 renderers=bar_renderers_energy)
    line_hover_actual = HoverTool(tooltips=tooltips_line_actual,
                                  renderers=[line_renderer_actual])
    line_hover_model = HoverTool(tooltips=tooltips_line_model,
                                 renderers=[line_renderer_model])
    p2.add_tools(bar_hover_energy, line_hover_actual, line_hover_model)

    # Add a checkbox button group to toggle visibility of bars
    toggle_button = Button(label="Toggle Bars", button_type="primary")
    toggle_callback = CustomJS(args=dict(renderers=bar_renderers_energy),
                               code="""
        // Loop through each renderer and toggle its visibility
        for (var i = 0; i < renderers.length; i++) {
            renderers[i].visible = !renderers[i].visible;
        }
        """)
    toggle_button.js_on_click(toggle_callback)

    # Assemble components
    layout_energy = column(p2, toggle_button)
    return layout_energy


def create_interval_energy_plot(system: dbc.LinearSystem,
                                solution: dbc.Solution,
                                tasks_data: dict) -> Column:
    solution_data = solution.solution_data
    solver = solution.method
    system_data = system.system_data
    system_id = system.id
    data_energy_model, \
        data_energy_cumulated_model, \
        data_energy_cumulated_real = prepare_data_energy(
            system_data, solution_data)
    layout_energy = generate_bokeh_energy_plot(system_id, solver,
                                               data_energy_model,
                                               data_energy_cumulated_model,
                                               data_energy_cumulated_real,
                                               tasks_data)
    return layout_energy


def create_interval_time_plot(system_name: str,
                              system_data: dict,
                              system_core_count: int,
                              sampling_period: int,
                              tasks_data: dict) -> Tuple[figure, dict]:
    data_times = prepare_data_times(system_data)
    plot = generate_bokeh_time_plot(system_name,
                                    data_times,
                                    sampling_period,
                                    system_core_count,
                                    tasks_data)
    return plot


def create_tasks_repartition_plot(system_name: str,
                                  system_data: dict,
                                  tasks_data: dict) -> figure:
    # Prepare data for the pie chart
    tasks = {task: {
        'duration': 0,
        'color': tasks_data[task]['Color']
    } for task in tasks_data.keys()}

    # Accumulate task times
    sum_duration = 0
    for interval_data in system_data.values():
        for task, task_time_ms in interval_data['TasksTotalTime'].items():
            if task in tasks:
                tasks[task]['duration'] += task_time_ms / 1000
                sum_duration += task_time_ms / 1000

    # Calculate the angle for each task
    for task in tasks.keys():
        tasks[task]['angle'] = tasks[task]['duration'] / \
            sum_duration * 2 * math.pi

    # Prepare the data for ColumnDataSource
    task_names = list(tasks.keys())
    durations = [tasks[task]['duration'] for task in task_names]
    colors = [tasks[task]['color'] for task in task_names]
    angles = [tasks[task]['angle'] for task in task_names]

    source = ColumnDataSource(data={
        'task': task_names,
        'duration': durations,
        'color': colors,
        'angle': angles
    })

    # Create the pie chart
    p = figure(height=TAB_HEIGHT,
               width=BOX_SIZE,
               title=f"Tasks Repartition for System {system_name}",
               toolbar_location=None, tools="hover",
               tooltips="@task: @duration",
               x_range=(-0.5, 0.5))

    p.wedge(x=0, y=1, radius=0.4,
            start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
            line_color="white", fill_color='color', source=source)

    p.axis.axis_label = None
    p.axis.visible = False
    p.grid.grid_line_color = None

    return p


def prepare_validation_data(residuals):
    # kde data
    kde = stats.gaussian_kde(residuals)
    x = np.linspace(min(residuals), max(residuals), 1000)
    kde_values = kde(x)
    kde_line_data = dict(x=x.tolist(),
                         y=kde_values.tolist())
    hist, edges = np.histogram(residuals, bins=30, density=True)
    kde_hist_data = dict(top=hist.tolist(),
                         left=edges[:-1].tolist(),
                         right=edges[1:].tolist())

    # qq plot data
    qq = stats.probplot(residuals, dist="norm", plot=None)
    qq_x = qq[0][0]
    qq_y = qq[0][1]
    slope, intercept, _ = qq[1]
    qq_scatter_data = dict(x=qq_x.tolist(),
                           y=qq_y.tolist())

    min_val = min(qq_x.min(), qq_y.min())
    max_val = max(qq_x.max(), qq_y.max())
    line_x = np.array([min_val, max_val])
    line_y = intercept + slope * line_x
    qq_line_data = dict(x=line_x.tolist(),
                        y=line_y.tolist())

    data_dict = {
        'kde_hist_data': kde_hist_data,
        'kde_line_data': kde_line_data,
        'qq_scatter_data': qq_scatter_data,
        'qq_line_data': qq_line_data
    }
    return data_dict


def create_validation_graphs(data_dict, system_id, solver):
    # Unpack data
    kde_hist_data = data_dict['kde_hist_data']
    kde_line_data = data_dict['kde_line_data']
    qq_scatter_data = data_dict['qq_scatter_data']
    qq_line_data = data_dict['qq_line_data']

    # KDE plot
    kde_name = f"{system_id}_kde_plot"
    kde_hist_source = ColumnDataSource(data=kde_hist_data)
    kde_plot = figure(title=f"Kernel density estimation plot ({solver})",
                      width=BOX_SIZE, height=BOX_SIZE,
                      name=kde_name)
    kde_plot.quad(top='top', bottom=0, left='left', right='right',
                  source=kde_hist_source, fill_color="red",
                  line_color="white", alpha=0.5,
                  legend_label="Residuals",
                  name="kde-histogram")

    kde_line_source = ColumnDataSource(data=kde_line_data)
    kde_plot.line('x', 'y', source=kde_line_source, line_width=2,
                  color='blue', legend_label="KDE",
                  name="kde-line")
    kde_plot.legend.location = "top_right"

    # QQ plot
    qq_name = f"{system_id}_qq_plot"

    qq_scatter_source = ColumnDataSource(data=qq_scatter_data)
    qq_plot = figure(title=f"Quantile-quantile plot ({solver})",
                     width=BOX_SIZE, height=BOX_SIZE,
                     name=qq_name)
    qq_plot.scatter('x', 'y', source=qq_scatter_source,
                    size=5, color='navy', alpha=0.5,
                    name="qq-scatter")

    qq_line_source = ColumnDataSource(data=qq_line_data)
    qq_plot.line('x', 'y', source=qq_line_source,
                 color='red', line_dash='dashed',
                 name="qq-line")

    qq_plot.xaxis.axis_label = 'Theoretical Quantiles'
    qq_plot.yaxis.axis_label = 'Ordered Values'

    # Arrange plots in tabs
    kde_panel = TabPanel(child=kde_plot, title="KDE Plot")
    qq_panel = TabPanel(child=qq_plot, title="QQ Plot")
    tabs = Tabs(tabs=[kde_panel, qq_panel], width=BOX_SIZE, height=BOX_SIZE)
    return tabs


def create_solution_legend(solution_dict, tasks_data):
    legend = {}
    for task, power in solution_dict.items():
        if tasks_data[task]['AvgDuration'] == None:
            avg_duration_str = "-"
            energy_str = "-"
        else:
            avg_duration_ms = tasks_data[task]['AvgDuration']
            energy_mJ = power * avg_duration_ms
            avg_duration_str = f"{avg_duration_ms:.4f} ms"
            energy_str = f"{energy_mJ:.4f} mJ"
        power_str = f"{power:.4f} W"
        color = tasks_data[task]['Color']
        entry = {
            'power': power_str,
            'color': color,
            'avg_duration': avg_duration_str,
            'energy': energy_str
        }
        legend[task] = entry
    return legend


def generate_experiment_graphs(experiment_name: str) -> Tuple[dict, str]:
    system_dict = {}
    try:
        systems = dbc.get_experiment_systems(experiment_name)
        if len(systems) == 0:
            raise PlottingException(
                f"No systems found for experiment {experiment_name}")
    except DatabaseException as e:
        raise PlottingException(f"Database error: {e}")
    bokeh_document_index = 0
    for system in systems:
        try:
            system_id = system.id
            system_name = system.system_name
            system_data = system.system_data
            system_core_count = system.core_count
            sampling_period = dbc.get_sampling_period(system_id)
            solution = dbc.get_initial_system_solution(system_id)
            tasks_data = system.tasks_data
        except DatabaseException as e:
            raise PlottingException(e)
        try:
            time_plot = create_interval_time_plot(system_name,
                                                  system_data,
                                                  system_core_count,
                                                  sampling_period,
                                                  tasks_data)
            pie_chart_time = create_tasks_repartition_plot(system_name,
                                                           system_data,
                                                           tasks_data)
            layout_time = row(pie_chart_time, time_plot)
            default_solver = solution.method
            energy_plot = create_interval_energy_plot(
                system, solution, tasks_data)
            solution_residuals = solution.residuals
            validation_data_dict = prepare_validation_data(solution_residuals)
            validation_graphs = create_validation_graphs(
                validation_data_dict, system_id, default_solver)
            validation = solution.metrics
            energy_plot.children[0].x_range = time_plot.x_range
            layout_energy = row(validation_graphs, energy_plot)
            script, divs_dict = components({"time_plot": layout_time,
                                            "energy_plot": layout_energy})
            system_name = system.system_name
            solution_dict = solution.solution_data
            legend_dict = create_solution_legend(solution_dict, tasks_data)
            system_dict[system_name] = {
                "script": script,
                "time_plot": divs_dict["time_plot"],
                "energy_plot": divs_dict["energy_plot"],
                "legend_dict": legend_dict,
                "validation": validation,
                "doc_index": bokeh_document_index
            }
            bokeh_document_index += 1
        except Exception as e:
            raise PlottingException(e)
    return system_dict, default_solver


def get_merged_solutions_graph(solution_ids: list) -> Tuple[str, str]:
    solutions: List[dbc.Solution] = [dbc.get_solution(
        solution_id) for solution_id in solution_ids]
    solutions_data: List[dict] = [
        solution.solution_data for solution in solutions]

    combined_tasks = defaultdict(list)
    for d in solutions_data:
        for task, power in d.items():
            combined_tasks[task].append(power)

    # Data formatting
    task_stats = {"task": [], "x_axis_label": [], "q1": [], "q3": [],
                  "mean": [], "stderr": [], "min": [], "max": [], "count": []}
    for task, power in combined_tasks.items():
        values_array = np.array(power)
        task_stats["task"].append(task)
        task_stats["count"].append(len(values_array))
        task_stats["mean"].append(np.mean(values_array))
        task_stats["stderr"].append(sem(values_array))
        task_stats["q1"].append(np.percentile(values_array, 25))
        task_stats["q3"].append(np.percentile(values_array, 75))
        task_stats["min"].append(np.min(values_array))
        task_stats["max"].append(np.max(values_array))
        task_stats["x_axis_label"].append(f"{task} ({len(values_array)})")
    source = ColumnDataSource(data=task_stats)

    # Boxplot elements
    p = figure(x_range=task_stats["x_axis_label"], title="Tasks power distribution",
               tools="pan,box_zoom,reset,save",
               toolbar_location="above",
               sizing_mode="stretch_both")
    p.vbar(x="x_axis_label", top="q3", bottom="q1", width=0.5,
           source=source, color="skyblue", legend_label="IQR")
    p.scatter(x="x_axis_label", y="mean", size=10, color="green",
              source=source, legend_label="Mean", marker="circle")
    p.add_layout(
        Whisker(source=source, base="x_axis_label", upper="max",
                lower="min", level="overlay", line_width=2)
    )

    # Wheel zoom tool
    wheel_zoom = WheelZoomTool(dimensions="height")
    p.add_tools(wheel_zoom)
    p.toolbar.active_scroll = wheel_zoom

    # Hover Tool
    hover = HoverTool()
    hover.tooltips = [
        ("Task", "@task"),
        ("Mean", "@mean{0.2f} W"),
        ("StdErr", "@stderr{0.2f} W"),
        ("Q1", "@q1{0.2f} W"),
        ("Q3", "@q3{0.2f} W"),
        ("Min", "@min{0.2f} W"),
        ("Max", "@max{0.2f} W"),
        ("Count", "@count"),
    ]
    hover.mode = 'vline'
    p.add_tools(hover)

    # Styling
    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = "lightgray"
    p.yaxis.axis_label = "Average power (W)"
    p.xaxis.axis_label = "Tasks"
    p.legend.location = "top_right"
    p.legend.click_policy = "hide"

    button = Button(label="Download CSV", button_type="success")
    button.js_on_click(CustomJS(args=dict(source=source), code="""
        const data = source.data;
        const columns = Object.keys(data);
        const csvRows = [];
        csvRows.push(columns.join(','));
        for (let i = 0; i < data[columns[0]].length; i++) {
            csvRows.push(columns.map(column => data[column][i]).join(','));
        }
        const blob = new Blob([csvRows.join('\\n')], { type: 'text/csv' });
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'task_stats.csv';
        a.click();
        URL.revokeObjectURL(url);
    """))
    layout = column(p, button, sizing_mode="stretch_both")

    # Embedding
    script, div = components(layout)
    return script, div


def compute_stats(values: List[float], tag_value: str) -> Dict[str, Any]:
    """
    Compute statistics for a list of values.
    """
    values_array = np.array(values)
    return {
        "tag_value": tag_value,
        "count": len(values_array),
        "mean": np.mean(values_array),
        "stderr": sem(values_array) if len(values_array) > 1 else 0,
        "q1": np.percentile(values_array, 25),
        "q3": np.percentile(values_array, 75),
        "min": np.min(values_array),
        "max": np.max(values_array),
        "x_axis_label": f"{tag_value} ({len(values_array)})"
    }


def format_combined_data(combined_data: Dict[str, List[float]]) -> Dict[str, List[Any]]:
    """
    Format combined data into a dictionary compatible with ColumnDataSource.
    """
    stats = defaultdict(list)
    for tag_value, values in combined_data.items():
        tag_stats = compute_stats(values, tag_value)
        for key, value in tag_stats.items():
            stats[key].append(value)
    return stats


def generate_tag_filtered_graph(source: ColumnDataSource,
                                tag_value_stats: dict,
                                task: str,
                                tag_key: str,
                                metric_name: str,
                                metric_unit: str) -> Tuple[str, str]:
    """
    Generate a tag-filtered graph with Bokeh.
    """
    if not tag_value_stats["tag_value"]:
        div = f"<div>No data available for metric '{metric_name}'</div>"
        script = "<script></script>"
        return (script, div)

    title = (f"{metric_name} box plot for task '{task}' "
             f"based on tag key '{tag_key}'")
    p = figure(x_range=tag_value_stats["x_axis_label"],
               title=title,
               tools="pan,box_zoom,reset,save",
               toolbar_location="above",
               sizing_mode="stretch_both")

    # Add boxplot elements
    p.vbar(x="x_axis_label", top="q3", bottom="q1", width=0.5,
           source=source, color="skyblue", legend_label="IQR")
    p.scatter(x="x_axis_label", y="mean", size=10, color="green",
              source=source, legend_label="Mean", marker="circle")
    p.add_layout(
        Whisker(source=source, base="x_axis_label", upper="max",
                lower="min", level="overlay", line_width=2)
    )

    # Add hover tool
    hover = HoverTool(tooltips=[
        ("Value", "@tag_value"),
        ("Mean", "@mean{0.2f}"),
        ("StdErr", "@stderr{0.2f}"),
        ("Q1", "@q1{0.2f}"),
        ("Q3", "@q3{0.2f}"),
        ("Min", "@min{0.2f}"),
        ("Max", "@max{0.2f}"),
        ("Count", "@count"),
    ])
    p.add_tools(hover)

    # Styling
    p.yaxis.axis_label = f"{metric_name} ({metric_unit})"
    p.xaxis.axis_label = "Tag value"
    p.legend.location = "top_right"
    p.legend.click_policy = "hide"

    # Add download button
    filename = f"{task}_{metric_name}_stats.csv"
    button = get_download_source_button(source, filename)
    layout = column(p, button, sizing_mode="stretch_both")

    return components(layout)


def generate_tag_filtered_graphs(system_ids: list[str],
                                 task: str,
                                 tag_key: str) -> Dict[str, Dict[str, str]]:
    """
    Generate graphs for power, energy, and time filtered by tags.
    """
    solutions: List[dbc.Solution] = [dbc.get_system_solution(system_id)
                                     for system_id in system_ids]
    combined_data = {"power-W": defaultdict(list),
                     "time-ms": defaultdict(list),
                     "energy-mJ": defaultdict(list),
                     "edp-mJ.ms": defaultdict(list)}

    for solution in solutions:
        tags = dbc.get_solution_tags(solution.id)
        tag_value = next(
            (tag.value for tag in tags if tag.key == tag_key), None)
        if tag_value is None or task not in solution.solution_data:
            continue
        task_power_J = solution.solution_data[task]
        combined_data["power-W"][tag_value].append(task_power_J)
        task_avg_time_ms = dbc.get_task_avg_duration(
            task, solution.linear_system_id)
        if task_avg_time_ms is not None:
            combined_data["time-ms"][tag_value].append(task_avg_time_ms)
            energy_mj = task_power_J * task_avg_time_ms
            combined_data["energy-mJ"][tag_value].append(energy_mj)
            edp_mJ_ms = energy_mj * task_avg_time_ms
            combined_data["edp-mJ.ms"][tag_value].append(edp_mJ_ms)

    # Format data sources
    results = {}

    for metric, data in combined_data.items():
        metric_name, metric_unit = metric.split("-")
        stats = format_combined_data(data)
        source = ColumnDataSource(data=stats)
        script, div = generate_tag_filtered_graph(
            source, stats, task, tag_key,
            metric_name, metric_unit)
        results[metric] = {"div": div, "script": script}
    return results


def main():
    return 0


if __name__ == '__main__':
    main()
