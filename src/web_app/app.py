#!/usr/bin/python
from flask import Flask, render_template, request, jsonify, redirect, url_for
from web_app.plots import generate_experiment_graphs, prepare_validation_data, create_solution_legend, generate_tag_filtered_graphs  # NOQA
import os
import sys

# Add the parent directory to the path
FILEDIR = os.path.dirname(os.path.abspath(__file__))
SRC_DIR = os.path.abspath(os.path.join(FILEDIR, ".."))
sys.path.append(SRC_DIR)

# Import custom modules
from web_app.plots import prepare_data_energy, get_merged_solutions_graph  # NOQA
from system_builder.build_system import SystemType  # NOQA
from system_solver.solve_system import solve_from_db, SolverType, compare_solution_and_system  # NOQA
from utils.custom_exceptions import DatabaseException, PlottingException  # NOQA
import database.database_connector as dbc  # NOQA

# Create the Flask app
app = Flask(__name__)


@app.route('/')
def index():
    try:
        databases = dbc.get_databases_in_data_dir()
        active_database = dbc.get_active_database()
        experiments = dbc.get_experiments(include_cluster=True)
        return render_template('index.html',
                               experiments=experiments,
                               databases=databases,
                               active_database=active_database)
    except DatabaseException as e:
        return f"Error: Could not retrieve experiments from database: {e}", 500


@app.route('/set_database', methods=['POST'])
def set_database():
    data = request.get_json()
    db = data['database_name']
    cur_db = dbc.get_active_database()
    print(cur_db)
    if (cur_db == db):
        return "", 204
    dbc.set_database_from_name(db)
    return redirect(url_for('index'))


@app.route('/delete_experiment', methods=['POST'])
def delete_experiment():
    data = request.get_json()
    experiment_id = data['experiment_id']
    try:
        dbc.remove_experiment(experiment_id)
    except DatabaseException as e:
        print(e)
        return jsonify(success=False), 404
    return jsonify(success=True)


@app.route('/experiment/<experiment_name>')
def show_experiment(experiment_name):
    try:
        databases = dbc.get_databases_in_data_dir()
        active_database = dbc.get_active_database()
        systems, default_solver = generate_experiment_graphs(experiment_name)
        solver_options = [solver.value for solver in SolverType]
        solver_options.remove(default_solver)
        return render_template('experiment.html',
                               systems=systems,
                               experiment_name=experiment_name,
                               solver_options=solver_options,
                               default_solver=default_solver,
                               databases=databases,
                               active_database=active_database)
    except PlottingException as e:
        return f"Error generating plots : {e}", 500


@app.route('/add_tag', methods=['POST'])
def add_tag():
    data = request.json
    experiment_id = data.get('experiment_id')
    key = data.get('key')
    value = data.get('value')
    if not experiment_id or not key or not value:
        return jsonify({'success': False, 'error': 'Invalid input data'}), 400
    try:
        dbc.insert_experiment_tag(experiment_id, key, value)
    except DatabaseException as e:
        return jsonify({'success': False, 'error': str(e)}), 500
    except FileExistsError as e:
        msg = f"Tag with key '{key}' already exists for experiment"
        return jsonify({'success': False, 'error': msg}), 409
    return jsonify({'success': True})


@app.route('/remove_tag', methods=['POST'])
def remove_tag():
    data = request.json
    experiment_id = data.get('experiment_id')
    tags = data.get('tags')
    if not experiment_id or not tags:
        return jsonify({'success': False, 'error': 'Invalid input data'}), 400
    try:
        for tag in tags:
            tag_key = tag.split(':')[0]
            tag_value = tag.split(':')[1]
            dbc.remove_experiment_tag(experiment_id, tag_key, tag_value)
        return jsonify({'success': True})
    except DatabaseException as e:
        return jsonify({'success': False, 'error': str(e)}), 500


@app.route('/update_solution', methods=['POST'])
def update_solution():
    try:
        request_options = request.get_json()
        solver = request_options['solver']
        experiment = request_options['experiment']
        systems_ids = dbc.get_experiments_systems_ids(experiment)
        if len(systems_ids) == 0:
            return "Error: No systems found for this experiment", 500
        # Check if the systems have been solved with the solver
        # We check the first system only since it should be all or nothing
        solution_exists = dbc.does_solution_exists(systems_ids[0], solver)
        if solution_exists == False:
            # solve the system
            solve_from_db(experiment, solver)
        # solution should always exist now for all systems
        response = {}
        for system_id in systems_ids:
            # Fetch the system and solution
            system = dbc.get_system(system_id)
            system_dict = system.system_data
            system_name = system.system_name
            solution = dbc.get_system_solution(system_id, solver)
            solution_dict = solution.solution_data

            # Create updated energy plot data
            data_energy_model, \
                data_energy_cumulated_model, _ = prepare_data_energy(
                    system_dict, solution_dict)

            # Create updated validation plots
            residuals = solution.residuals
            validation_data_dict = prepare_validation_data(residuals)
            kde_hist_data = validation_data_dict['kde_hist_data']
            kde_line_data = validation_data_dict['kde_line_data']
            qq_scatter_data = validation_data_dict['qq_scatter_data']
            qq_line_data = validation_data_dict['qq_line_data']

            # Create updated validation stats
            validation_metrics = solution.metrics

            # Create updated legend list
            legend_dict = create_solution_legend(
                solution_dict, system.tasks_data)

            # Build response
            response[system_id] = {
                'system_name': system_name,
                'energy_model': data_energy_model,
                'energy_cumul_model': data_energy_cumulated_model,
                'kde_hist_data': kde_hist_data,
                'kde_line_data': kde_line_data,
                'qq_scatter_data': qq_scatter_data,
                'qq_line_data': qq_line_data,
                'legend_dict': legend_dict,
                'validation_metrics': validation_metrics
            }
        return jsonify(response)
    except Exception as e:
        print(e)
        return f"Error updating energy plot : {e}", 500


@app.route('/compare', methods=['GET'])
def compare():
    try:
        databases = dbc.get_databases_in_data_dir()
        active_database = dbc.get_active_database()
        hosts = dbc.get_hosts_ids()
        system_types = [system.value for system in SystemType]
        solver_methods = [solver.value for solver in SolverType]
        tags = dbc.get_tags()
        solutions_ids = dbc.get_solutions_ids()
        solutions_ids_tags = [(sol_id, dbc.get_solution_tags(sol_id))
                              for sol_id in solutions_ids]
        solutions_ids_tags.sort()
    except DatabaseException as e:
        return f"Error on compare page : {e}", 500
    return render_template('compare.html',
                           hosts=hosts,
                           system_types=system_types,
                           solver_methods=solver_methods,
                           tags=tags,
                           solutions_ids_tags=solutions_ids_tags,
                           databases=databases,
                           active_database=active_database)


@app.route('/get_filtered_solutions', methods=['POST'])
def get_filtered_solutions():
    data: dict[str, str] = request.json
    host = data.get('host', None)
    system_type = data.get('system_type', None)
    solver_method = data.get('solver_method', None)
    tag_key = data.get('tag_key', None)
    tag_value = data.get('tag_value', None)
    # Handle sending with base option
    if host == "Select Host":
        host = None
    if system_type == "Select System Type":
        system_type = None
    if solver_method == "Select Solver Method":
        solver_method = None
    if tag_key == "Select Tag":
        tag_key = None
    solutions_ids = dbc.get_filtered_solutions(
        host, system_type, solver_method, tag_key, tag_value)
    solutions_ids_tags = [
        (sol_id, [{"key": tag.key, "value": tag.value}
         for tag in dbc.get_solution_tags(sol_id)])
        for sol_id in solutions_ids
    ]
    solutions_ids_tags.sort()
    return jsonify(solutions_ids_tags)


@app.route('/get_filtered_linear_systems', methods=['POST'])
def get_filtered_linear_systems():
    data: dict[str, str] = request.json
    host = data.get('host', None)
    system_type = data.get('system_type', None)
    tag_key = data.get('tag_key', None)
    # Handle sending with base option
    if host == "Select Host":
        host = None
    if system_type == "Select System Type":
        system_type = None
    if tag_key == "Select Tag":
        tag_key = None
    systems_ids = dbc.get_filtered_linear_systems(host, system_type, tag_key)
    system_ids_tags = [
        (sys_id, [{"key": tag.key, "value": tag.value}
                  for tag in dbc.get_linear_system_tags(sys_id)])
        for sys_id in systems_ids]
    return jsonify(system_ids_tags)


@app.route('/get_comparison_systems', methods=['POST'])
def get_comparison_systems():
    solution_id = request.json['solution']
    compatible_systems_ids = dbc.get_compatible_systems_ids(solution_id)
    return jsonify(compatible_systems_ids)


@app.route('/compare_systems', methods=['POST'])
def compare_systems():
    data = request.get_json()
    solution_id = data['solution']
    system_ids = data['systems']
    results = []
    for system_id in system_ids:
        dict_metrics = compare_solution_and_system(solution_id, system_id)
        results.append({'system': system_id,
                        'rmsd': dict_metrics['rmsd'],
                        'mape': dict_metrics['mape'],
                        'r_squared': dict_metrics['r_squared'],
                        'norm_residuals': dict_metrics['norm_residuals'],
                        'perc_residuals': dict_metrics['perc_residuals']})
    return jsonify(results)


@app.route('/merge_solutions', methods=['POST'])
def merge_solutions():
    data = request.get_json()
    solution_ids = data['solutions']
    try:
        merged_script, merged_div = get_merged_solutions_graph(solution_ids)
    except Exception as e:
        return jsonify({'success': False, 'error': str(e)}), 500
    return jsonify({'success': True,
                    'merged_script': merged_script,
                    'merged_div': merged_div})


@app.route('/get_tag_values', methods=['POST'])
def get_tag_values():
    data = request.get_json()
    key = data.get('tag_key', None)
    values = dbc.get_tag_values(key)
    return jsonify(values)


@app.route('/get_systems_common_tasks', methods=['POST'])
def get_systems_common_tasks():
    data = request.get_json()
    systems_ids = data.get('systems', None)
    tasks = dbc.get_systems_common_tasks(systems_ids)
    return jsonify(tasks)


@app.route('/compare_solutions_by_tag', methods=['POST'])
def compare_solutions_by_tag():
    data = request.get_json()
    systems = data.get('systems', None)
    task = data.get('task', None)
    tag_key = data.get('tag_key', None)
    try:
        graph_dict = generate_tag_filtered_graphs(systems, task, tag_key)
        power_div = graph_dict['power-W']['div']
        power_script = graph_dict['power-W']['script']
        energy_div = graph_dict['energy-mJ']['div']
        energy_script = graph_dict['energy-mJ']['script']
        time_div = graph_dict['time-ms']['div']
        time_script = graph_dict['time-ms']['script']
        edp_div = graph_dict['edp-mJ.ms']['div']
        edp_script = graph_dict['edp-mJ.ms']['script']
    except Exception as e:
        return jsonify({'success': False, 'error': str(e)}), 500
    return jsonify({'success': True,
                    'power_div': power_div,
                    'power_script': power_script,
                    'energy_div': energy_div,
                    'energy_script': energy_script,
                    'time_div': time_div,
                    'time_script': time_script,
                    'edp_div': edp_div,
                    'edp_script': edp_script})


if __name__ == '__main__':
    app.run(debug=True)
