// static/scripts.js

document.addEventListener("DOMContentLoaded", function () {
  const pageIdentifier = document.getElementById("page-identifier");
  if (!pageIdentifier) {
    console.error("Page identifier not found.");
  }
  const page = pageIdentifier.getAttribute("data-page");
  // common event : DB change
  document
    .getElementById("setDatabaseButton")
    .addEventListener("click", function () {
      const databaseName = document.getElementById("databaseSelect").value;
      fetch("/set_database", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          database_name: databaseName,
        }),
      })
        .then((response) => {
          if (response.status === 204) {
            console.log("Database is already selected. No action needed.");
            return;
          }
          window.location.href = response.url;
        })
        .catch((error) => {
          console.error("Error:", error);
          alert("Failed to set database.");
        });
    });
  // index events
  if (page === "index") {
    document.querySelectorAll(".add-tag-badge").forEach(function (button) {
      button.addEventListener("click", function () {
        const parentDiv = this.closest(".list-group-item");
        const experimentId = parentDiv.getAttribute("data-experiment-id");
        document.getElementById("experimentModalId").value = experimentId;
        const modal = new bootstrap.Modal(
          document.getElementById("addTagModal")
        );
        modal.show();
      });
    });
    document
      .getElementById("insertTagButton")
      .addEventListener("click", function () {
        const experimentId = document.getElementById("experimentModalId").value;
        const tagKey = document.getElementById("tagKey").value;
        const tagValue = document.getElementById("tagValue").value;
        fetch("/add_tag", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            experiment_id: experimentId,
            key: tagKey,
            value: tagValue,
          }),
        })
          .then((response) => response.json())
          .then((data) => {
            if (data.success) {
              const modal = bootstrap.Modal.getInstance(
                document.getElementById("addTagModal")
              );
              modal.hide();
              const parentDiv = document.querySelector(
                `[data-experiment-id="${experimentId}"]`
              );
              const badge = document.createElement("span");
              badge.className = "badge bg-secondary rounded-pill me-1";
              badge.textContent = `${tagKey}: ${tagValue}`;
              parentDiv.querySelector(".add-tag-badge").before(badge);
            } else {
              alert("Failed to add tag: " + data.error);
            }
          })
          .catch((error) => {
            console.error("Error:", error);
            alert("Failed to add tag.");
          });
      });
    document
      .querySelectorAll(".delete-experiment-btn")
      .forEach(function (button) {
        button.addEventListener("click", function () {
          const confirmed = confirm(
            "Are you sure you want to delete this experiment?"
          );
          if (!confirmed) {
            return;
          }
          const listItem = this.closest(".list-group-item");
          const experimentId = listItem.querySelector("a").textContent.trim();
          fetch("/delete_experiment", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ experiment_id: experimentId }),
          })
            .then((response) => response.json())
            .then((data) => {
              if (data.success) {
                window.location.reload();
              } else {
                console.error("Failed to delete experiment.");
              }
            })
            .catch((error) => {
              console.error("Error:", error);
            });
        });
      });
    document.querySelectorAll(".remove-tag-badge").forEach(function (button) {
      button.addEventListener("click", function () {
        const parentDiv = this.closest(".list-group-item");
        const experimentId = parentDiv.getAttribute("data-experiment-id");
        document.getElementById("removeExperimentModalId").value = experimentId;
        const tagsList = parentDiv.querySelectorAll(".badge.bg-secondary");
        const tagsContainer = document.getElementById("tagsList");
        tagsContainer.innerHTML = "";
        tagsList.forEach((tagBadge) => {
          const tagText = tagBadge.textContent.trim();
          const checkbox = document.createElement("div");
          checkbox.className = "form-check";
          checkbox.innerHTML = `
                        <input class="form-check-input" type="checkbox" value="${tagText}" id="tag-${tagText}">
                        <label class="form-check-label" for="tag-${tagText}">${tagText}</label>
                    `;
          tagsContainer.appendChild(checkbox);
        });
        const modal = new bootstrap.Modal(
          document.getElementById("removeTagModal")
        );
        modal.show();
      });
    });
    document
      .getElementById("removeTagButton")
      .addEventListener("click", function () {
        const experimentId = document.getElementById(
          "removeExperimentModalId"
        ).value;

        // Get all selected tags
        const selectedTags = Array.from(
          document.querySelectorAll("#tagsList .form-check-input:checked")
        ).map((checkbox) => checkbox.value);

        if (selectedTags.length === 0) {
          alert("Please select at least one tag to remove.");
          return;
        }

        fetch("/remove_tag", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            experiment_id: experimentId,
            tags: selectedTags,
          }),
        })
          .then((response) => response.json())
          .then((data) => {
            if (data.success) {
              const modal = bootstrap.Modal.getInstance(
                document.getElementById("removeTagModal")
              );
              modal.hide();

              // Find the parent div for the experiment
              const parentDiv = document.querySelector(
                `[data-experiment-id="${experimentId}"]`
              );

              // Remove the corresponding badges
              selectedTags.forEach((tagText) => {
                const badges = parentDiv.querySelectorAll(
                  ".badge.bg-secondary"
                );
                badges.forEach((badge) => {
                  if (badge.textContent.trim() === tagText) {
                    badge.remove();
                  }
                });
              });

              // If no tags remain, hide the remove button
              if (
                parentDiv.querySelectorAll(".badge.bg-secondary").length === 0
              ) {
                const removeButton =
                  parentDiv.querySelector(".remove-tag-badge");
                removeButton.style.visibility = "hidden";
              }
            } else {
              alert("Failed to remove tags: " + data.error);
            }
          })
          .catch((error) => {
            console.error("Error:", error);
            alert("Failed to remove tags.");
          });
      });
  }
  // experiment events
  else if (page === "experiment") {
    const updateButton = document.getElementById("updateButton");
    const solverSelect = document.getElementById("solverSelect");
    const experimentName = document.getElementById("experimentName").value;

    function update_solution_list(dict, system_name, selectedSolver) {
      // Update title
      const table_title_id = system_name + "-solution-title";
      const table_title = document.getElementById(table_title_id);
      if (!table_title) {
        console.error("Table title not found:", table_title_id);
        return;
      }
      table_title.textContent = "Results (" + selectedSolver + ")";
      // Update list
      for (const task_name in dict) {
        const task_dict = dict[task_name];
        const row_id = `${system_name}-${task_name}-solution-item`;
        const row = document.getElementById(row_id);
        if (!row) {
          console.error("Row not found:", row_id);
          continue;
        }
        // Update power
        const power_cell = row.querySelector(".power-cell");
        if (power_cell) {
          power_cell.textContent = task_dict.power;
        }
        // Update energy
        const energy_cell = row.querySelector(".energy-cell");
        if (energy_cell) {
          energy_cell.textContent = task_dict.energy;
        }
      }
    }

    updateButton.addEventListener("click", function () {
      const selectedSolver = solverSelect.value;
      // Create the request payload
      const payload = {
        solver: selectedSolver,
        experiment: experimentName,
      };
      // Fetch new data based on selected solver
      fetch("/update_solution", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error("Network response was not ok");
          }
          return response.json();
        })
        .then((data) => {
          Object.keys(data).forEach((system_id) => {
            // Update the energy plot sources
            const system_data = data[system_id];
            const system_name = system_data.system_name;
            const bokeh_index_name = system_name + "-bokeh-index";
            const bokeh_index_value = parseInt(
              document.getElementById(bokeh_index_name).value
            );
            const figure_name = system_id + "_energy";
            const figure_energy =
              Bokeh.documents[bokeh_index_value].get_model_by_name(figure_name);
            if (figure_energy) {
              figure_energy.title.text =
                "Estimated and real energy consumption for solver " +
                selectedSolver;
              const energy_renderers = figure_energy.renderers;
              for (let i = 0; i < energy_renderers.length; i++) {
                const renderer = energy_renderers[i];
                if (renderer.tags.includes("bar-renderer-energy-tag")) {
                  renderer.data_source.data = system_data.energy_model;
                  renderer.data_source.change.emit();
                } else if (renderer.tags.includes("line-renderer-model-tag")) {
                  renderer.data_source.data = system_data.energy_cumul_model;
                  renderer.data_source.change.emit();
                }
              }
              figure_energy.change.emit();
            } else {
              console.error(`Figure with name ${figure_name} not found.`);
            }
            // Update the kde plot sources
            const figure_kde_name = system_id + "_kde_plot";
            const figure_kde =
              Bokeh.documents[bokeh_index_value].get_model_by_name(
                figure_kde_name
              );
            if (figure_kde) {
              figure_kde.title.text =
                "Kernel density estimation plot (" + selectedSolver + ")";
              const kde_renderers = figure_kde.renderers;
              for (let i = 0; i < kde_renderers.length; i++) {
                const renderer = kde_renderers[i];
                if (renderer.name === "kde-histogram") {
                  renderer.data_source.data = system_data.kde_hist_data;
                  renderer.data_source.change.emit();
                } else if (renderer.name === "kde-line") {
                  renderer.data_source.data = system_data.kde_line_data;
                  renderer.data_source.change.emit();
                }
              }
              figure_kde.change.emit();
            } else {
              console.error(`Figure with name ${figure_kde_name} not found.`);
            }
            // Update the qq plot sources
            const figure_qq_name = system_id + "_qq_plot";
            const figure_qq =
              Bokeh.documents[bokeh_index_value].get_model_by_name(
                figure_qq_name
              );
            if (figure_qq) {
              figure_qq.title.text =
                "Quantile-quantile plot (" + selectedSolver + ")";
              const qq_renderers = figure_qq.renderers;
              for (let i = 0; i < qq_renderers.length; i++) {
                const renderer = qq_renderers[i];
                if (renderer.name === "qq-scatter") {
                  renderer.data_source.data = system_data.qq_scatter_data;
                  renderer.data_source.change.emit();
                } else if (renderer.name === "qq-line") {
                  renderer.data_source.data = system_data.qq_line_data;
                  renderer.data_source.change.emit();
                }
              }
              figure_qq.change.emit();
            } else {
              console.error(`Figure with name ${figure_qq_name} not found.`);
            }
            // Update the solution list
            const solution_list = system_data.legend_dict;
            update_solution_list(solution_list, system_name, selectedSolver);

            // Update validation metrics
            const validation_title_id = system_name + "-validation-title";
            const validation_title =
              document.getElementById(validation_title_id);
            if (!validation_title) {
              console.error(
                "Validation title not found : ",
                validation_title_id
              );
              return;
            }
            validation_title.textContent =
              "Validation (" + selectedSolver + ")";
            const validation_metrics = system_data.validation_metrics;
            const validation_body_id = system_name + "-validation-body";
            const validation_body = document.getElementById(validation_body_id);
            if (!validation_body) {
              console.error("Validation body not found : ", validation_body_id);
              return;
            }
            validation_body.innerHTML = validation_metrics;
          });
        })
        .catch((error) => {
          console.error("Error updating solution:", error);
        });
    });
  } else if (page === "compare") {
    // CONSTANTS

    // Solution comparator constants
    const hostSelect = document.getElementById("hostSelect");
    const systemTypeSelect = document.getElementById("systemTypeSelect");
    const solverMethodSelect = document.getElementById("solverMethodSelect");
    const tagSelect = document.getElementById("tagSelect");
    const resultsSelect = document.getElementById("resultsSelect");
    const resultsSelectSize = document.getElementById("resultsSelectSize");
    const compareSelect = document.getElementById("compareSelect");
    const compareSelectSize = document.getElementById("compareSelectSize");
    const resultsTable = document
      .getElementById("resultsTable")
      .querySelector("tbody");
    const filterButton = document.getElementById("filterButton");
    const compareButton = document.getElementById("compareButton");
    const selectAllButton = document.getElementById("selectAllButton");
    const unselectAllButton = document.getElementById("unselectAllButton");

    // Merger constants
    const hostSelectMerger = document.getElementById("hostSelectMerger");
    const systemTypeSelectMerger = document.getElementById(
      "systemTypeSelectMerger"
    );
    const solverMethodSelectMerger = document.getElementById(
      "solverMethodSelectMerger"
    );
    const tagSelectMerger = document.getElementById("tagSelectMerger");
    const mergerFilterButton = document.getElementById("mergerFilterButton");
    const mergerResultsSelect = document.getElementById("mergerResultsSelect");
    const mergeButton = document.getElementById("mergeButton");
    const mergerResultsSelectSize = document.getElementById(
      "mergerResultsSelectSize"
    );
    const mergerSelectAllButton = document.getElementById(
      "mergerSelectAllButton"
    );
    const mergerUnselectAllButton = document.getElementById(
      "mergerUnselectAllButton"
    );

    // Tag solution comparator constants
    const hostSelectProgram = document.getElementById("hostSelectProgram");
    const systemTypeSelectProgram = document.getElementById(
      "systemTypeSelectProgram"
    );
    const tagKeySelectProgram = document.getElementById("TagKeySelectProgram");
    const TagValuesProgram = document.getElementById("TagValuesProgram");
    const filterButtonProgram = document.getElementById("filterButtonProgram");
    const resultsSelectProgram = document.getElementById(
      "resultsSelectProgram"
    );
    const resultsSelectProgramSize = document.getElementById(
      "resultsSelectProgramSize"
    );
    const TaskSelectProgram = document.getElementById("TaskSelectProgram");
    const compareButtonProgram = document.getElementById(
      "compareButtonProgram"
    );
    const tagSelectAllButton = document.getElementById("tagSelectAllButton");
    const tagUnselectAllButton = document.getElementById(
      "tagUnselectAllButton"
    );

    // FUNCTIONS

    // Solution comparator functions

    // Select all and unselect all buttons
    function selectAll() {
      for (const option of compareSelect.options) {
        option.selected = true;
      }
      compareSelect.dispatchEvent(new Event("change"));
    }
    function unselectAll() {
      for (const option of compareSelect.options) {
        option.selected = false;
      }
      compareSelect.dispatchEvent(new Event("change"));
    }

    // Result filtering
    function updateResults() {
      const host = hostSelect.value;
      const systemType = systemTypeSelect.value;
      const solverMethod = solverMethodSelect.value;
      const tag = tagSelect.value;
      const [tag_key, tag_value] = tag.split(":");
      fetch("/get_filtered_solutions", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          host: host,
          system_type: systemType,
          solver_method: solverMethod,
          tag_key: tag_key,
          tag_value: tag_value,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          resultsSelect.innerHTML = "";
          data.forEach(([solution, tags]) => {
            const option = document.createElement("option");
            option.value = solution;
            option.textContent = `${solution} - ${tags
              .map((tag) => `${tag.key}:${tag.value}`)
              .join(", ")}`;
            resultsSelect.appendChild(option);
          });
          resultsSelectSize.textContent = data.length;
        });
    }

    // Update comparison systems
    function updateComparisonSystems() {
      const selectedSolution = resultsSelect.value;
      if (selectedSolution) {
        fetch("/get_comparison_systems", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ solution: selectedSolution }),
        })
          .then((response) => response.json())
          .then((data) => {
            compareSelect.innerHTML = "";
            data.forEach((system) => {
              const option = document.createElement("option");
              option.value = system;
              option.textContent = system;
              compareSelect.appendChild(option);
            });
            compareSelectSize.textContent = data.length;
          });
      }
    }

    // Apply solution to selected systems
    function compareSystems() {
      const selectedSolution = resultsSelect.value;
      const selectedSystems = Array.from(compareSelect.selectedOptions).map(
        (option) => option.value
      );
      if (selectedSolution && selectedSystems.length > 0) {
        fetch("/compare_systems", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            solution: selectedSolution,
            systems: selectedSystems,
          }),
        })
          .then((response) => response.json())
          .then((data) => {
            resultsTable.innerHTML = "";
            data.forEach((result) => {
              const row = document.createElement("tr");
              row.innerHTML = `<td>${result.system}</td>
                               <td>${result.rmsd}</td>
                               <td>${result.mape}</td>
                               <td>${result.r_squared}</td>
                               <td>${result.norm_residuals}</td>
                               <td>${result.perc_residuals}</td>`;
              resultsTable.appendChild(row);
            });
          });
      }
    }

    // Merger functions

    // Select all and unselect all buttons
    function mergerSelectAll() {
      for (const option of mergerResultsSelect.options) {
        option.selected = true;
      }
      mergerResultsSelect.dispatchEvent(new Event("change"));
    }
    function mergerUnselectAll() {
      for (const option of mergerResultsSelect.options) {
        option.selected = false;
      }
      mergerResultsSelect.dispatchEvent(new Event("change"));
    }

    // Filter results for merging
    function updateMergerResults() {
      const host = hostSelectMerger.value;
      const systemType = systemTypeSelectMerger.value;
      const solverMethod = solverMethodSelectMerger.value;
      const tag = tagSelectMerger.value;
      const [tag_key, tag_value] = tag.split(":");
      fetch("/get_filtered_solutions", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          host: host,
          system_type: systemType,
          solver_method: solverMethod,
          tag_key: tag_key,
          tag_value: tag_value,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          mergerResultsSelect.innerHTML = "";
          data.forEach(([solution, tags]) => {
            const option = document.createElement("option");
            option.value = solution;
            option.textContent = `${solution} - ${tags
              .map((tag) => `${tag.key}:${tag.value}`)
              .join(", ")}`;
            mergerResultsSelect.appendChild(option);
          });
          mergerResultsSelectSize.textContent = data.length;
        });
    }

    // Show merged solution results
    function mergeSolutions() {
      const selectedSolutions = Array.from(
        mergerResultsSelect.selectedOptions
      ).map((option) => option.value);
      if (selectedSolutions.length > 0) {
        fetch("/merge_solutions", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ solutions: selectedSolutions }),
        })
          .then((response) => response.json())
          .then((data) => {
            if (data.success) {
              const graphContainer = document.getElementById(
                "mergedGraphContainer"
              );
              graphContainer.innerHTML = "";
              graphContainer.innerHTML = data.merged_div;
              const scriptContent = data.merged_script.match(
                /<script.*?>([\s\S]*?)<\/script>/
              )[1];
              const scriptTag = document.createElement("script");
              scriptTag.textContent = scriptContent;
              document.body.appendChild(scriptTag);
            } else {
              alert(`Error: ${data.error}`);
            }
          })
          .catch((error) => {
            console.error("Error:", error);
            alert("Failed to merge solutions.");
          });
      }
    }

    // Tag solution comparator functions

    // Select all and unselect all buttons
    function tagSelectAll() {
      for (const option of resultsSelectProgram.options) {
        option.selected = true;
      }
      resultsSelectProgram.dispatchEvent(new Event("change"));
    }

    function tagUnselectAll() {
      for (const option of resultsSelectProgram.options) {
        option.selected = false;
      }
      resultsSelectProgram.dispatchEvent(new Event("change"));
    }

    function updateTagKeySelectProgram() {
      const tag_key = tagKeySelectProgram.value;
      fetch("/get_tag_values", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ tag_key: tag_key }),
      })
        .then((response) => response.json())
        .then((data) => {
          TagValuesProgram.innerHTML = "";
          data.forEach((tag_value) => {
            const span = document.createElement("span");
            span.className = "badge bg-secondary rounded-pill me-1";
            span.textContent = tag_value;
            TagValuesProgram.appendChild(span);
          });
        });
    }

    function updateResultsProgramComparator() {
      const host = hostSelectProgram.value;
      const systemType = systemTypeSelectProgram.value;
      const tagKey = tagKeySelectProgram.value;
      if (tagKey === "Select Tag Key") {
        alert("Please select a tag key.");
        return;
      }
      fetch("/get_filtered_linear_systems", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          host: host,
          system_type: systemType,
          tag_key: tagKey,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          resultsSelectProgram.innerHTML = "";
          data.forEach(([system, tags]) => {
            const option = document.createElement("option");
            option.value = system;
            option.textContent = `${system} - ${tags
              .map((tag) => `${tag.key}:${tag.value}`)
              .join(", ")}`;
            resultsSelectProgram.appendChild(option);
          });
          resultsSelectProgramSize.textContent = data.length;
        });
    }

    function getLinearSystemCommonTasks() {
      const systems = Array.from(resultsSelectProgram.selectedOptions).map(
        (option) => option.value
      );
      return fetch("/get_systems_common_tasks", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ systems: systems }),
      })
        .then((response) => response.json())
        .then((data) => {
          TaskSelectProgram.innerHTML = "";
          data.forEach((task) => {
            const option = document.createElement("option");
            option.value = task;
            option.textContent = task;
            TaskSelectProgram.appendChild(option);
          });
        });
    }

    function compareSystemsByTag() {
      const selectedSystems = Array.from(
        resultsSelectProgram.selectedOptions
      ).map((option) => option.value);
      const selectedTask = TaskSelectProgram.value;
      const selectedTagKey = tagKeySelectProgram.value;
      if (selectedSystems.length > 0 && selectedTask) {
        fetch("/compare_solutions_by_tag", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            systems: selectedSystems,
            task: selectedTask,
            tag_key: selectedTagKey,
          }),
        })
          .then((response) => response.json())
          .then((data) => {
            if (data.success) {
              // Power graph
              const powerGraphContainer = document.getElementById(
                "programPowerGraphContainer"
              );
              powerGraphContainer.innerHTML = "";
              powerGraphContainer.innerHTML = data.power_div;
              const powerGraphScriptContent = data.power_script.match(
                /<script.*?>([\s\S]*?)<\/script>/
              )[1];
              const powerGraphScript = document.createElement("script");
              powerGraphScript.textContent = powerGraphScriptContent;
              document.body.appendChild(powerGraphScript);

              // Time graph
              const programTimeGraphContainer = document.getElementById(
                "programTimeGraphContainer"
              );
              programTimeGraphContainer.innerHTML = "";
              programTimeGraphContainer.innerHTML = data.time_div;
              const timeGraphScriptContent = data.time_script.match(
                /<script.*?>([\s\S]*?)<\/script>/
              )[1];
              const timeGraphScript = document.createElement("script");
              timeGraphScript.textContent = timeGraphScriptContent;
              document.body.appendChild(timeGraphScript);

              // Energy graph
              const energyGraphContainer = document.getElementById(
                "programEnergyGraphContainer"
              );
              energyGraphContainer.innerHTML = "";
              energyGraphContainer.innerHTML = data.energy_div;
              const energyGraphScriptContent = data.energy_script.match(
                /<script.*?>([\s\S]*?)<\/script>/
              )[1];
              const energyGraphScript = document.createElement("script");
              energyGraphScript.textContent = energyGraphScriptContent;
              document.body.appendChild(energyGraphScript);

              // EDP graph
              const edpGraphContainer = document.getElementById(
                "programEdpGraphContainer"
              );
              edpGraphContainer.innerHTML = "";
              edpGraphContainer.innerHTML = data.edp_div;
              const edpGraphScriptContent = data.edp_script.match(
                /<script.*?>([\s\S]*?)<\/script>/
              )[1];
              const edpGraphScript = document.createElement("script");
              edpGraphScript.textContent = edpGraphScriptContent;
              document.body.appendChild(edpGraphScript);
            } else {
              alert(`Error: ${data.error}`);
            }
          })
          .catch((error) => {
            console.error("Error:", error);
            alert("Failed to compare programs.");
          });
      }
    }

    // EVENT LISTENERS

    // Solution comparator events
    filterButton.addEventListener("click", updateResults);
    resultsSelect.addEventListener("change", updateComparisonSystems);
    compareButton.addEventListener("click", compareSystems);
    selectAllButton.addEventListener("click", selectAll);
    unselectAllButton.addEventListener("click", unselectAll);

    // Merger events
    mergerFilterButton.addEventListener("click", updateMergerResults);
    mergeButton.addEventListener("click", mergeSolutions);
    mergerSelectAllButton.addEventListener("click", mergerSelectAll);
    mergerUnselectAllButton.addEventListener("click", mergerUnselectAll);

    // Tag solution comparator events
    tagKeySelectProgram.addEventListener("change", updateTagKeySelectProgram);
    filterButtonProgram.addEventListener(
      "click",
      updateResultsProgramComparator
    );
    compareButtonProgram.addEventListener("click", compareSystemsByTag);
    tagSelectAllButton.addEventListener("click", tagSelectAll);
    tagUnselectAllButton.addEventListener("click", tagUnselectAll);
    resultsSelectProgram.addEventListener("change", getLinearSystemCommonTasks);
  }
});
