#!/bin/bash

# Verify and extract arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <input_directory> <output_directory>"
    exit 1
fi
input_directory=$1
output_directory=$2

# Define the input and output files
input_filename="topo.rec"
output_filename="topo.csv"
input_file="$input_directory/$input_filename"
output_file="$output_directory/$output_filename"

# Check if files exists
if [ ! -f "$input_file" ]; then
    echo "Error: $input_file does not exist."
    exit 1
fi

# Start by writing the header to the output file
echo "Package,Core,Worker,Type,Device" > "$output_file"

# Use awk to process the file and format it as CSV
awk 'BEGIN {
    FS=", "; OFS=","; # Set input and output field separators
}
{
    for (i = 1; i <= NF; i++) {
        split($i, kv, ": ");
        key = kv[1];
        value = kv[2];
        
        if (key == "package") package = value;
        else if (key == "core") core = value;
        else if (key == "worker") worker = value;
        else if (key == "type") type = value;
        else if (key == "device") device = value;
    }
    print package, core, worker, type, device;
}' "$input_file" >> "$output_file"
