#!/usr/bin/python

from utils.custom_exceptions import DatabaseException, BuilderException
import database.database_connector as dbc
import os
import sys
import pandas as pd
import numpy as np
import json
import logging
import datetime
from enum import Enum
from bokeh.palettes import inferno

# Add the parent directory to the path
FILEDIR = os.path.dirname(os.path.abspath(__file__))
SRC_DIR = os.path.abspath(os.path.join(FILEDIR, ".."))
sys.path.append(SRC_DIR)

# Import custom modules

# Logging basic configuration
logger = logging.getLogger('system_builder_logger')
logger.setLevel(logging.INFO)
console_handler = logging.StreamHandler(sys.stdout)
console_handler.setLevel(logging.INFO)
logger.addHandler(console_handler)

# Global constants
ROUND_VALUE = 6
WORKDIR = ''

# FINE TUNING PARAMETERS
# Filter tasks accounting for less than (x*100)% of the total time measured
PERC_FILTER_CONSTANT = float(os.environ.get('PERC_FILTER_CONSTANT', 0))
ACCOUNT_SIZE = os.environ.get('ACCOUNT_SIZE', 'False').lower() == 'true'
GROUP_PKG_SYSTEMS = os.environ.get(
    'GROUP_PKG_SYSTEMS', 'False').lower() == 'true'

# Enum for the system type


class SystemType(Enum):
    CPU = "CPU"
    GPU = "GPU"


def configure_logger():
    """
    Configures the logger for the builder application.

    This function sets up the logger to log messages to both the console and a log file.
    It creates a log file with a timestamped name in the specified output directory.

    Returns:
        None
    """
    if WORKDIR == '':
        logger.error(
            'Please set the output directory before configuring the logger.')
        sys.exit(1)
    now = datetime.datetime.now()
    logfile_name = f'build_system_{now.strftime("%Y-%m-%d_%H-%M-%S")}.log'
    logfile_path = os.path.join(WORKDIR, logfile_name)
    file_handler = logging.FileHandler(logfile_path, 'w')
    file_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s\n%(message)s')
    console_handler.setFormatter(formatter)
    file_handler.setFormatter(formatter)
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)


def count_PUs(df_topo):
    """
    Count the number of processing units (PUs) in the given dataframe.

    Parameters:
    - df_topo: pandas DataFrame
        The dataframe containing the topology information of the PUs.

    Returns:
    - PU_count_dict: dict
        A dictionary containing the count of PUs for each system type.
        Also contains the total count of PUs for PKG systems.
    """
    PU_count_dict = {}
    cpu_workers = df_topo[df_topo['Type'] == 'CPU']['Package'].unique()
    sum_cpu_cores = 0
    for pkg in cpu_workers:
        name = 'PKG_' + str(pkg)
        PU_count_dict[name] = df_topo[df_topo['Package']
                                      == pkg]['Core'].count()
        sum_cpu_cores += PU_count_dict[name]
    PU_count_dict['PKG'] = sum_cpu_cores
    gpu_workers = df_topo[df_topo['Type'] == 'CUDA']['Device'].unique()
    for gpu in gpu_workers:
        name = 'GPU_' + str(gpu)
        PU_count_dict[name] = 1
    return PU_count_dict


def prepare_dataframes(builder_result_dir):
    """
    Prepare the DataFrames from the CSV files in the specified directory.

    Args:
        builder_result_dir (str): The directory path where the CSV files are located.

    Returns:
        tuple: A tuple containing three pandas DataFrames: df_energy, df_trace, and df_topo.
            - df_energy: DataFrame containing energy data.
            - df_trace: DataFrame containing trace data.
            - df_topo: DataFrame containing topology data.
    """
    # Check if the directory exists
    if not os.path.isdir(builder_result_dir):
        logger.error('The directory', builder_result_dir, 'does not exist.')
        sys.exit(1)
    # Check if the CSV files exist
    for csv_file in ['tasks.csv', 'energy.csv', 'topo.csv', 'trace.csv']:
        if not os.path.isfile(os.path.join(builder_result_dir, csv_file)):
            logger.error('The file', csv_file,
                         'does not exist in', builder_result_dir)
            sys.exit(1)

    # Open CSV's
    df_tasks = pd.read_csv(os.path.join(builder_result_dir, 'tasks.csv'))
    df_energy = pd.read_csv(os.path.join(builder_result_dir, 'energy.csv'))
    df_topo = pd.read_csv(os.path.join(builder_result_dir, 'topo.csv'))
    df_trace = pd.read_csv(os.path.join(builder_result_dir, 'trace.csv'))

    # Complete the energy dataframe
    df_energy['Timestamp'] = df_energy['Timestamp'].astype(float)
    df_energy['BeginTimestamp'] = df_energy.groupby('Type')[
        'Timestamp'].shift(1)
    df_energy['EndTimestamp'] = df_energy['Timestamp']
    df_energy['BeginTimestamp'] = df_energy['BeginTimestamp'].fillna(0)
    df_energy['Duration'] = df_energy['EndTimestamp'] - \
        df_energy['BeginTimestamp']
    df_energy['Power'] = df_energy['Value'] / (df_energy['Duration'] / 1000)
    # Dropping the first measure of each type
    df_energy = df_energy[df_energy['BeginTimestamp'] != 0]
    df_energy.drop('Timestamp', axis=1, inplace=True)
    # Adding a unique ID to each interval
    df_energy.reset_index(drop=True, inplace=True)
    df_energy['ID'] = df_energy.index + 1

    # Complete the trace dataframe
    df_trace['Duration'] = df_trace['EndTime'] - df_trace['StartTime']
    df_trace = df_trace[df_trace['WorkerID'] >= 0]
    merged_df = pd.merge(df_trace, df_tasks[['WorkerId', 'Name', 'StartTime', 'Sizes']],
                         how='left',
                         left_on=['WorkerID', 'EventName', 'StartTime'],
                         right_on=['WorkerId', 'Name', 'StartTime'])
    merged_df.drop(['WorkerId', 'Name'], axis=1, inplace=True)
    merged_df = pd.merge(merged_df, df_topo[['Worker', 'Type', 'Device', 'Package']],
                         how='left',
                         left_on='WorkerID',
                         right_on='Worker')
    merged_df.drop('Worker', axis=1, inplace=True)
    merged_df['Device'] = merged_df['Device'].astype(int)

    # Set the Type & Device columns
    # Tasks on GPU go to the GPU system, rest goes to their package system
    merged_df['Type'] = np.where(
        (merged_df['Type'] == 'CUDA') & (merged_df['EventCategory'] == 'Task'),
        'GPU_' + merged_df['Device'].astype(str),
        'PKG_' + merged_df['Package'].astype(str)
    )

    # Set the Variable column based on mutliple conditions
    global ACCOUNT_SIZE
    negliged_states = ['Initializing', 'Deinitializing', 'End', 'Executing',
                       'stop_profiling', 'start_profiling', 'Callback', 'execute_on_all_wrapper_GPU']
    merged_df['ShortType'] = merged_df['Type'].str.split('_').str[0]

    # First condition, do we take sizes into account ?
    if ACCOUNT_SIZE:
        merged_df['Variable'] = np.where(
            # Check if it's a task
            merged_df['EventCategory'] == 'Task',
            # If it's a task, use the name + size + type
            merged_df['EventName'] + '_' +
            merged_df['Sizes'].astype(str) + '_' + merged_df['ShortType'],
            # If not a task, check if it's in negliged states
            np.where(
                merged_df['EventName'].isin(negliged_states),
                'Sleeping' + '_' + merged_df['ShortType'],
                merged_df['EventName'] + '_' + merged_df['ShortType']
            )
        )
    else:
        merged_df['Variable'] = np.where(
            # Check if it's a task
            merged_df['EventCategory'] == 'Task',
            # If it's a task, use the name + type
            merged_df['EventName'] + '_' + merged_df['ShortType'],
            # If not a task, check if it's in negliged states
            np.where(
                merged_df['EventName'].isin(negliged_states),
                'Sleeping' + '_' + merged_df['ShortType'],
                merged_df['EventName'] + '_' + merged_df['ShortType']
            )
        )

    merged_df['Variable'] = merged_df['Variable'].str.replace(' ', '_')
    df_trace = merged_df
    return df_energy, df_trace, df_topo


def print_infos(df_energy, df_trace, df_topo):
    """
    Prints information about tasks, energy, topology, and system.
    Informations are printed and logged.

    Args:
        df_energy (pandas.DataFrame): DataFrame containing energy information.
        df_trace (pandas.DataFrame): DataFrame containing trace information.
        df_topo (pandas.DataFrame): DataFrame containing topology information.

    Returns:
        None
    """
    pd.set_option('display.max_columns', None)
    pd.set_option('display.max_rows', None)

    # Precompute groupby and describe values
    task_descriptions = df_trace.groupby('Variable')['Duration'] \
        .describe().to_string()
    task_sums_sorted = df_trace.groupby('Variable')['Duration'] \
        .sum().sort_values(ascending=False).to_string()
    energy_descriptions = df_energy.groupby('Type')['Value'] \
        .describe().to_string()
    core_counts = df_topo.groupby('Package')['Core'] \
        .count().to_string()
    worker_counts = df_topo[df_topo['Worker'] >= 0] \
        .groupby('Package')['Worker'].count().to_string()

    # Construct the information string used for logging
    infos_str = (
        '-------------------- TASKS --------------------\n' +
        f'{task_descriptions}'
        '\nTasks cumulated duration : \n' +
        f'{task_sums_sorted}'
        '\n-------------------- ENERGY --------------------\n' +
        f'{energy_descriptions}'
        '\n-------------------- TOPO --------------------\n'
        'Core count per ' +
        f'{core_counts}'
        '\nWorker count per ' +
        f'{worker_counts}')
    infos_str += '\n-------------------- SYSTEM --------------------\n'
    for system_type in df_trace['Type'].unique():
        infos_str += f'{system_type}\n'
        tasks = df_trace[df_trace['Type'] == system_type]['Variable'].unique()
        nunique_tasks = len(tasks)
        infos_str += f'\t Variables : {nunique_tasks}\n'
        infos_str += f'\t {tasks}\n'
        num_equations = df_energy[df_energy['Type'] == system_type].shape[0]
        infos_str += f'\t Equations : {num_equations}\n'
    infos_str += '\n'
    logger.info(infos_str)


def filter_tasks(perc, df_trace):
    """
    Group tasks that account for less than a certain percentage of the total time measured
    into a single 'OtherTasks' variable.

    Args:
        perc (float): The percentage threshold below which tasks will be filtered out.
        df_trace (pandas.DataFrame): The DataFrame containing the trace data.

    Returns:
        pandas.DataFrame: The modified DataFrame with tasks below the threshold
        grouped under 'OtherTasks' + Type.
    """
    if 'Variable' not in df_trace.columns:
        raise ValueError("Column 'Variable' does not exist in the DataFrame.")

    # Calculate total time and percentage of time spent per task
    total_time = df_trace['Duration'].sum()
    df_trace['TimePerc'] = df_trace.groupby(
        'Variable')['Duration'].transform(lambda x: x.sum() / total_time)

    # Identify tasks to be grouped based on the percentage criterion
    # don't filter out GPU tasks
    tasks_to_modify = \
        df_trace[(df_trace['TimePerc'] < perc)
                 & (df_trace['EventCategory'] == 'Task')
                 & (df_trace['ShortType'] == 'PKG')]['Variable'].unique()

    # Log information about the tasks to be grouped
    filtered_time = df_trace[df_trace['Variable'].isin(
        tasks_to_modify)]['Duration'].sum()
    filtered_time_perc = (filtered_time / total_time) * 100
    filter_str = '-------------------- FILTER --------------------\n'
    filter_str += (
        'Filtered out tasks that account for less than '
        f'{str(perc * 100)}% of the total time measured. \n'
        f'Total time filtered out: {round(filtered_time, 2)} ms'
        f'({round(filtered_time_perc, 2)} %)\n')
    filter_str += 'Tasks to be grouped : \n' + str(tasks_to_modify) + '\n'

    # Modify the 'Variable' value to 'OtherTasks' + Type for tasks below the threshold
    mask_tasks = df_trace['Variable'].isin(tasks_to_modify)
    df_trace.loc[mask_tasks, 'Variable'] = 'OtherTasks' + \
        '_' + df_trace.loc[mask_tasks, 'ShortType']

    # Log the filtering results
    logger.info(filter_str)

    # drop time percentage column
    df_trace.drop('TimePerc', axis=1, inplace=True)
    return df_trace


def prepare_system(df_energy, df_topo, df_trace):
    """
    Create the linear system dictionary representation for each system type.
    The keys are the system types, and the values are dictionaries
    containing the energy intervals and their corresponding data.
    Can be easily exported to a JSON file for visualization.

    Args:
        df_energy (pandas.DataFrame): DataFrame containing energy data.
        df_topo (pandas.DataFrame): DataFrame containing system topology data.
        df_trace (pandas.DataFrame): DataFrame containing trace data.

    Returns:
        dict: A dictionary containing the prepared systems representations.
    """
    system_list = df_trace['Type'].unique()
    results = {system: {} for system in system_list}
    systems_tasks_data = {system: {} for system in system_list}

    # Build the tasks data dictionary
    for system in systems_tasks_data:
        tasks = df_trace[df_trace['Type'] == system]['Variable'].unique()
        for task in tasks:
            task_durations = df_trace[(df_trace['Type'] == system) &
                                      (df_trace['Variable'] == task) &
                                      (df_trace['EventCategory'] == 'Task')]['Duration']
            task_avg_time = task_durations.mean()
            rounded_avg_time = round(
                task_avg_time, ROUND_VALUE) if task_avg_time is not np.nan else None
            systems_tasks_data[system][task] = {
                'AvgDuration': rounded_avg_time}

    # Pre-filter df_energy to include only relevant types
    df_energy = df_energy[df_energy['Type'].isin(system_list)]

    # Process each energy interval
    for idx, energy_interval in df_energy.iterrows():
        interval_id = 'interval_' + str(energy_interval['ID'])
        system_type = energy_interval['Type']
        system_short_type = system_type.split('_')[0]
        devid = int(system_type.split('_')[1])

        # Setup common interval data
        interval_data = {
            'Energy': energy_interval['Value'],
            'Power': energy_interval['Power'],
            'BeginTimestamp': energy_interval['BeginTimestamp'],
            'EndTimestamp': energy_interval['EndTimestamp'],
            'Duration': energy_interval['Duration'],
            'TasksTotalTime': {}
        }

        if system_type.startswith('PKG'):
            # CPU Pkg systems
            nb_pus = df_topo[df_topo['Package'] == devid]['Core'].count()
            nworkers = df_topo[(df_topo['Package'] == devid)
                               & (df_topo['Worker'] >= 0)]['Worker'].count()
        else:
            # Default for GPU systems
            nb_pus = nworkers = 1

        interval_data['ExpectedTotalTime'] = energy_interval['Duration'] * nb_pus
        measured_time = 0

        # NotWorkers time
        if nworkers < nb_pus:
            non_worker_time = (nb_pus - nworkers) * energy_interval['Duration']
            non_worker_name = 'Sleeping' + '_' + system_short_type
            interval_data['TasksTotalTime'][non_worker_name] = non_worker_time
            # Add that custom task to the systems_tasks_data
            if systems_tasks_data[system_type].get(non_worker_name) is None:
                systems_tasks_data[system_type][non_worker_name] = {
                    'AvgDuration': None}
            measured_time += non_worker_time

        # Filter trace data for this interval
        relevant_traces = df_trace[
            (df_trace['Type'] == system_type) &
            (df_trace['EndTime'] > energy_interval['BeginTimestamp']) &
            (df_trace['StartTime'] < energy_interval['EndTimestamp'])].copy()

        # Calculate time spent in interval
        relevant_traces['TimeInInterval'] = np.minimum(relevant_traces['EndTime'],
                                                       energy_interval['EndTimestamp']) - \
            np.maximum(relevant_traces['StartTime'],
                       energy_interval['BeginTimestamp'])

        # Group and sum by 'Variable'
        time_by_variable = relevant_traces.groupby(
            'Variable')['TimeInInterval'].sum()
        for variable, duration in time_by_variable.items():
            interval_data['TasksTotalTime'].setdefault(variable, 0)
            interval_data['TasksTotalTime'][variable] += duration
        measured_time += time_by_variable.sum()

        # If GPU system, rest of the time is spent idle
        if system_type.startswith('GPU'):
            idle_time = energy_interval['Duration'] - measured_time
            idle_name = 'Idle' + '_' + system_short_type
            interval_data['TasksTotalTime'][idle_name] = idle_time
            # Add that custom task to the systems_tasks_data
            if systems_tasks_data[system_type].get(idle_name) is None:
                systems_tasks_data[system_type][idle_name] = {
                    'AvgDuration': None}
            measured_time += idle_time

        interval_data['MeasuredTotalTime'] = measured_time
        results[system_type][interval_id] = interval_data

    # Add color palette to systems_tasks_data
    for system in systems_tasks_data:
        tasks = systems_tasks_data[system].keys()
        color_palette = inferno(len(tasks))
        for i, task in enumerate(tasks):
            systems_tasks_data[system][task]['Color'] = color_palette[i]
    return results, systems_tasks_data


def json_dump(results, trace_directory, filename='system.json'):
    """
    Dump the results systems as a JSON string to a file.

    Args:
        results (dict): The results systems to be dumped as JSON.
        trace_directory (str): The directory where the JSON file will be saved.
        filename (str): The name of the JSON file.

    Returns:
        None
    """
    json_results = json.dumps(results, indent=4)
    output_file = os.path.join(trace_directory, filename)
    with open(output_file, 'w') as f:
        f.write(json_results)
    return


def read_key_value_file(exp_directory, file, separator='='):
    """
    Read a key-value file and return its content as a dictionary.

    Args:
        exp_directory (str): The directory where the file is located.
        file (str): The name of the file to read.
        separator (str): The separator character between the key and value, default is '='.

    Returns:
        dict: A dictionary containing the key-value pairs from the file.

    """
    dic = {}
    try:
        file_path = os.path.join(exp_directory, file)
        with open(file_path, 'r') as f:
            for line in f:
                key, value = line.strip().split(separator, maxsplit=1)
                dic[key] = value
    except Exception as e:
        raise BuilderException(
            f'Error while reading file {file_path} : {e}')
    return dic


def merge_systems_tasks_data(systems_tasks_data):
    # Initialize the merged PKG dictionary
    merged_tasks_data = {}
    # Iterate over the systems in systems_tasks_data
    for system, tasks_data in systems_tasks_data.items():
        if system.startswith('PKG'):
            for task, data in tasks_data.items():
                if task not in merged_tasks_data:
                    merged_tasks_data[task] = {
                        'AvgDuration': [], 'Color': None}
                merged_tasks_data[task]['AvgDuration'].append(
                    data['AvgDuration'])
    # Calculate the average duration for each task in the merged PKG
    for task, data in merged_tasks_data.items():
        durations = data['AvgDuration']
        if any(duration is None for duration in durations):
            merged_tasks_data[task]['AvgDuration'] = None
        else:
            merged_tasks_data[task]['AvgDuration'] = np.mean(durations)
    # Regenerate the colors using the inferno palette from bokeh
    color_palette = inferno(len(merged_tasks_data))
    for color, (task, data) in zip(color_palette, merged_tasks_data.items()):
        merged_tasks_data[task]['Color'] = color
    # Create the new systems_tasks_data with the merged PKG entry
    new_systems_tasks_data = {'PKG': merged_tasks_data}
    # Add back the non-PKG entries
    for system, tasks_data in systems_tasks_data.items():
        if not system.startswith('PKG'):
            new_systems_tasks_data[system] = tasks_data
    return new_systems_tasks_data


def main():
    """
    Entry point of the script.

    This function takes the experience root directory as a
    command-line argument and performs the following steps:

    1. Prepare the Dataframes from the CSV directory.
    2. Filter out tasks that are rarely executed.
    3. Log information about tasks, energy, topology, and system.
    4. Build the dictionary representing the system.
    5. Export the system (JSON file & database).

    Args:
        None

    Returns:
        None
    """
    # Arg is the experience directory
    if (len(sys.argv) < 2):
        logger.error('Usage: python build_system.py <exp_directory>')
        sys.exit(1)

    # Set the input directory
    exp_directory = sys.argv[1]
    if not os.path.isdir(exp_directory):
        logger.error('The directory', exp_directory, 'does not exist.')
        sys.exit(1)
    exp_directory = os.path.normpath(exp_directory)
    exp_directory = os.path.realpath(exp_directory)

    builder_result_dir = os.path.join(exp_directory, 'builder_results')
    if not os.path.exists(builder_result_dir):
        logger.error('The directory', builder_result_dir, 'does not exist.')
        sys.exit(1)

    # Set the output directory
    global WORKDIR
    WORKDIR = builder_result_dir
    configure_logger()

    # Verify the database connection
    if dbc.does_database_exist() is False:
        logger.warning("Database does not exist, creating it.")
        try:
            dbc.create_database()
        except Exception as e:
            logger.error(f'Error while creating the database : {e}')
            sys.exit(1)

    # Process parameter files
    try:
        with open(os.path.join(exp_directory, 'environment.json'), 'r') as f:
            env_variables: dict[str, str] = json.load(f)
        with open(os.path.join(exp_directory, 'tags.json'), 'r') as f:
            tags: dict[str, str] = json.load(f)
        host_infos = read_key_value_file(exp_directory, 'host_infos.txt')
        energy_recap = read_key_value_file(exp_directory, 'energy_recap.txt')
        experiment_infos = read_key_value_file(
            exp_directory, 'experiment_infos.txt')
    except BuilderException as e:
        logger.error(f'Error while processing parameter files : {e}')
        sys.exit(1)

    # Insert cluster in the database
    cluster_id = host_infos.get('CLUSTER_NAME')
    if cluster_id is None:
        logger.error('CLUSTER_NAME not found in the host_infos.txt file.')
        sys.exit(1)
    try:
        dbc.try_insert_cluster(cluster_id)
    except DatabaseException as e:
        logger.error(
            f'Error while inserting cluster {cluster_id} in the database : {e}')
        sys.exit(1)

    # Insert host in the database
    host_id = host_infos.get('HOST_NAME')
    if host_id is None:
        logger.error('HOST_NAME not found in the host_infos.txt file.')
        sys
    cpu = host_infos.get('CPU_MODEL', None)
    gpu = host_infos.get('GPU_MODEL', None)
    ram = host_infos.get('RAM', None)
    try:
        dbc.try_insert_host(host_id, cpu, gpu, ram, cluster_id)
    except DatabaseException as e:
        logger.error(
            f'Error while inserting host {host_id} in the database : {e}')
        sys.exit(1)

    # Insert experiment in the database
    exp_id = os.path.basename(exp_directory)
    exp_program = experiment_infos.get('PROGRAM')
    exp_duration = experiment_infos.get('DURATION')
    rapl_sample_interval_ms = experiment_infos.get('RAPL_SAMPLE_INTERVAL_MS')
    gpu_sample_interval_ms = experiment_infos.get('GPU_SAMPLE_INTERVAL_MS')
    sample_intervals = {
        'RAPL_SAMPLE_INTERVAL_MS': rapl_sample_interval_ms,
        'GPU_SAMPLE_INTERVAL_MS': gpu_sample_interval_ms
    }
    try:
        dbc.insert_experiment(exp_id, exp_program, host_id, env_variables,
                              exp_duration, sample_intervals, energy_recap, tags)
    except DatabaseException as e:
        logger.error(
            f'Error while inserting experiment {exp_id} in the database : {e}')
        sys.exit(1)

    # Prepare the DF's
    df_energy, df_trace, df_topo = prepare_dataframes(builder_result_dir)

    # Filter out tasks that account for less than a % of the total task time measured
    df_trace = filter_tasks(PERC_FILTER_CONSTANT, df_trace)

    # Log information about tasks, energy, topology, and system
    print_infos(df_energy, df_trace, df_topo)

    # Save the processed trace & energy dataframes
    df_energy.to_csv(os.path.join(builder_result_dir,
                     'energy_processed.csv'), index=False)
    df_trace.to_csv(os.path.join(builder_result_dir,
                    'trace_processed.csv'), index=False)

    # Build the system
    system_dict, systems_tasks_data = prepare_system(
        df_energy, df_topo, df_trace)

    # If GROUP_PKG_SYSTEMS is set, group all CPU systems together
    if GROUP_PKG_SYSTEMS:
        # Merging systems data is straightforward since the values (intervals) are disjoint
        pkg_keys = [
            system for system in system_dict if system.startswith('PKG')]
        for system in pkg_keys:
            if 'PKG' not in system_dict:
                system_dict['PKG'] = {}
            system_dict['PKG'].update(system_dict[system])
            del system_dict[system]
        # Merging tasks data is trickier since the keys (tasks) are not disjoint
        systems_tasks_data = merge_systems_tasks_data(systems_tasks_data)

    # Export the system and tasks data to a JSON file
    json_dump(system_dict, builder_result_dir, 'system.json')
    json_dump(systems_tasks_data, builder_result_dir, 'tasks.json')

    # insert systems in the database
    dic_PUs = count_PUs(df_topo)
    for system_name in system_dict.keys():
        if system_name.startswith('PKG'):
            system_type = SystemType.CPU.value
        elif system_name.startswith('GPU'):
            system_type = SystemType.GPU.value
        else:
            logger.error(f'Unknown system type for system {system_name}.')
            sys.exit(1)
        system_id = f'{exp_id}_{system_name}'
        system_data = system_dict[system_name]
        core_count = int(dic_PUs[system_name])
        tasks_data = systems_tasks_data[system_name]
        try:
            dbc.insert_system(system_id,
                              exp_id,
                              system_name,
                              system_type,
                              system_data,
                              core_count,
                              PERC_FILTER_CONSTANT,
                              tasks_data)
        except DatabaseException as e:
            logger.error(
                f'Error while inserting system {system_id} in the database : {e}')
            sys.exit(1)
        finally:
            logger.info(f'System {system_id} inserted in the database.')


if __name__ == '__main__':
    main()
