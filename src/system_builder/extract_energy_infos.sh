#!/bin/bash

# Verify and extract arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <input_directory> <output_directory>"
    exit 1
fi
input_directory=$1
output_directory=$2

# Define the input and output files
input_filename="energy.rec"
output_filename="energy.csv"
input_file="$input_directory/$input_filename"
output_file="$output_directory/$output_filename"

# Check if files exists
if [ ! -f "$input_file" ]; then
    echo "Error: $input_file does not exist."
    exit 1
fi

# Start by writing the header to the output file
echo "Type,Value,Timestamp" > "$output_file"

# Use awk to process the file and format it as CSV
awk '
BEGIN {
    FS=": "; OFS=","; # Set field separator to ": " and output field separator to ","
}

/Type:|Value:|Timestamp:/ {
    split($0, a, ": ");
    if(a[1]=="Type") {
        gsub(/ /, "_", a[2]);
        type=a[2];
    }
    else if(a[1]=="Value") {
        gsub(/ J$/, "", a[2]);
        value=a[2];
    }
    else if(a[1]=="Timestamp") {
        timestamp=a[2];
        print type,value,timestamp;
        type=""; value=""; timestamp="";
    }
}' "$input_file" >> "$output_file"
