#!/bin/bash

# Verify and extract arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <input_directory> <output_directory>"
    exit 1
fi
input_directory=$1
output_directory=$2

# Define the input and output files
input_filename="tasks.rec"
output_filename="tasks.csv"
input_file="$input_directory/$input_filename"
output_file="$output_directory/$output_filename"

# Check if files exists
if [ ! -f "$input_file" ]; then
    echo "Error: $input_file does not exist."
    exit 1
fi

# Writing the header to the output file
echo "Name,JobId,WorkerId,EnergyCoreId,EnergyPackageId,EnergyWorkerArch,EnergyDeviceId,StartTime,EndTime,Parameters,Sizes" > "$output_file"

# Using awk to process the file and format it as CSV, 
# Filtering out extra chameleon entries without Name and Model values
awk '
BEGIN {
    FS=": "; OFS=","; # Set field separator to ": " and output field separator to ","
    namePresent = 0; # Initialize flag for Name presence
    modelPresent = 0; # Initialize flag for Model presence
}

{
    split($0, a, ": ");
    if(a[1]=="Name") {name=a[2]; namePresent = 1}
    else if(a[1]=="Model") {model=a[2]; modelPresent = 1}
    else if(a[1]=="JobId") {jobId=a[2]}
    else if(a[1]=="WorkerId") {workerId=a[2]}
    else if(a[1]=="EnergyCoreId") {energyCoreId=a[2]}
    else if(a[1]=="EnergyPackageId") {energyPackageId=a[2]}
    else if(a[1]=="EnergyWorkerArch") {energyWorkerArch=a[2]}
    else if(a[1]=="EnergyDeviceId") {energyDeviceId=a[2]}
    else if(a[1]=="StartTime") {startTime=a[2]}
    else if(a[1]=="EndTime") {endTime=a[2]}
    else if(a[1]=="Parameters") {parameters=a[2]}
    else if(a[1]=="Sizes") {sizes=a[2]}
}

/^$/ {
    if(namePresent && modelPresent) {
        print name,jobId,workerId,energyCoreId,energyPackageId,energyWorkerArch,energyDeviceId,startTime,endTime,parameters,sizes
    }
    name=""; jobId=""; workerId=""; energyCoreId=""; energyPackageId=""; energyWorkerArch=""; energyDeviceId=""; startTime=""; endTime=""; parameters=""; sizes=""
    namePresent = 0; modelPresent = 0;
}' "$input_file" >> "$output_file"
