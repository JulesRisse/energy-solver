#!/bin/bash

# Verify and extract arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <input_directory> <output_directory>"
    exit 1
fi
input_directory=$1
output_directory=$2

# Define the input and output files
input_filename="trace.rec"
output_filename="trace.csv"
input_file="$input_directory/$input_filename"
output_file="$output_directory/$output_filename"

# Check if files exist
if [ ! -f "$input_file" ]; then
    echo "Error: $input_file does not exist."
    exit 1
fi

# Process the file and format it as CSV
awk '
BEGIN {
    FS=": "; OFS=","; # Set field and output field separators
    print "EventName,EventCategory,WorkerID,ThreadID,StartTime,EndTime"; # Print header
}

# Store information for each worker and update when state changes
/^E:/ { state = $2 }
/^N:/ { newState = $2 }
/^C:/ { category = $2 }
/^W:/ { workerId = $2 }
/^T:/ { threadId = $2 }
/^S:/ {
    startTime = $2;

    # Prints if the worker has a previous state
    if (workerId in workerState && workerId in startTimeRecord && workerId in categoryRecord) {
        print workerState[workerId], categoryRecord[workerId], workerId, threadRecord[workerId], startTimeRecord[workerId], startTime;
    }

    # Update records for the current worker (unless it is the end state)
    if (newState != "End") {
        workerState[workerId] = newState;
        startTimeRecord[workerId] = startTime;
        categoryRecord[workerId] = category;
        threadRecord[workerId] = threadId;
    }
}
' "$input_file" > "$output_file"
